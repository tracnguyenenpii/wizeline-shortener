<?php
namespace frontend\controllers;

use common\libs\ClsControllerFrontend;



/**
 * Site controller
 */
class SiteController extends ClsControllerFrontend
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->redirect(['shortener-item/create']);
        return $this->render('index', [
        ]);
    }

    public function actionError()
    {
        return $this->render('error', [
            'message' => '404 page not found',
        ]);

    }

}
