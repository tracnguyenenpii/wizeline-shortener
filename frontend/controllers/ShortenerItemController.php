<?php

namespace frontend\controllers;

use common\models\query\ShortenerItemQuery;
use Yii;
use common\models\ShortenerItem;
use common\models\ShortenerItemSearch;
use common\libs\ClsController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use enpii\enpiiCms\helpers\ArrayHelper;
use enpii\enpiiCms\helpers\DateTimeHelper;
use enpii\enpiiCms\helpers\CommonHelper;

/**
 * ShortenerItemController implements the CRUD actions for ShortenerItem model.
 */
class ShortenerItemController extends ClsController
{
    public function behaviors()
    {
        $parentOneBehaviors = parent::behaviors();
        $thisOneBehaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
        return ArrayHelper::merge($parentOneBehaviors, $thisOneBehaviors);
    }

    /**
     * Lists all ShortenerItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShortenerItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRedirect() {
        $slug = !empty($_REQUEST['slug']) ? $_REQUEST['slug'] : '';
        /* @var $model ShortenerItem */
        $model = ShortenerItem::findOne(['slug' => $slug]);
        if (!empty($model->slug)) {
            return $this->redirect($model->original_url);
        } else {
            die('Slug does not exist');
        }

    }

    /**
     * Displays a single ShortenerItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Create a new ShortenerItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShortenerItem();

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->slug)) {
                $model->slug = uniqid();
            }
            $model->created_at = DateTimeHelper::toDbFormat();
            $model->updated_at = DateTimeHelper::toDbFormat();


            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Data created successfully.'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Data created unsuccessfully. Please recheck the input.'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShortenerItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = DateTimeHelper::toDbFormat();

            if ($model->status == ShortenerItem::_STATUS_PUBLISHED) {
                $model->published_at = DateTimeHelper::toDbFormat();
            }

            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Data updated successfully.'));
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Data updated unsuccessfully. Please recheck the input.'));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShortenerItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->putToTrash()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Data deleted successfully.'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Data deleted unsuccessfully.'));
        }

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        return $this->redirect($indexUrlQueryParams);
    }

    /**
     * Finds the ShortenerItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShortenerItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShortenerItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested item does not exist.'));
        }
    }

    /**
    * Bulk action on selected items
    * @return \yii\web\Response
    */
    public function actionBulk()
    {
        $action = Yii::$app->request->post('bulk-option');
        $selection = (array)Yii::$app->request->post('selection');

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        if (count($selection) > 0) {
            $arrIDSuccess = [];
            $arrIDFail = [];

            foreach ($selection as $id) {
                $result = false;
                if ($action == 'delete') {
                    $strAction = Yii::t('app', 'Execute Bulk Deletion.');
                    $result = $this->findModel($id)->putToTrash();
                }
                if ($result) {
                    $arrIDSuccess[] = $id;
                } else {
                    $arrIDFail[] = $id;
                }
            }

            $strAction = empty($strAction) ? '' : $strAction . ' ';

            if (count($arrIDSuccess) > 0) {
                Yii::$app->session->addFlash('success',
                $strAction . Yii::t('app', 'Total {num} item(s) successfully affected: {items}.', [
    'num' => 'count($arrIDSuccess)',
    'items' => 'implode(\', \', $arrIDSuccess)',
]));
            }

            if (count($arrIDFail) > 0) {
                Yii::$app->session->addFlash('error', $strAction . Yii::t('app', 'Total {num} item(s) failed: {items}.', [
    'num' => 'count($arrIDFail)',
    'items' => 'implode(\', \', $arrIDFail)',
]));
            }

        } else {
            Yii::$app->session->addFlash('warning', Yii::t('app', 'No items selected'));
        }

        return $this->redirect($indexUrlQueryParams);
    }
}
