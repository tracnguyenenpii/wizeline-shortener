<?php
namespace frontend\controllers;

use common\helper\StringHelper;
use common\libs\ClsControllerFrontend;
use common\models\CouponPin;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

/**
 * Site controller
 */
class OfferController extends ClsControllerFrontend
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
        ]);
    }

    public function actionSavePin($id) {

        $id = StringHelper::base64UrlDecode($id);
        $couponPin = $this->findModel($id);
        if($couponPin->is_obtained) {
            throw new NotFoundHttpException(Yii::t('app', 'The coupon is no longer viewable'));
        }

        return $this->render('view-coupon', [
            'model' => $couponPin
        ]);
    }

    protected function findModel($id)
    {
        if (($model = CouponPin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested item does not exist.'));
        }
    }

    public function actionViewImage($id) {

        $id = StringHelper::base64UrlDecode($id);
        $this->layout = false;
        $model = $this->findModel($id);

        if($model->is_obtained) {
            throw new NotFoundHttpException(Yii::t('app', 'The coupon is no longer viewable'));
        }

        $model->prepareCouponPin();
        $model->save();

        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'image/png');
        $response->format = Response::FORMAT_RAW;

        return $this->renderPartial('image-coupon',[
            'model' => $model
        ]);
    }

}
