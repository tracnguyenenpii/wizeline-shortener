<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/13/16 3:15 PM
 */

namespace frontend\assets;

use enpii\enpiiCms\libs\NpAssetBundle;

class MainCustomAsset extends NpAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main-custom.css',
    ];
    public $js = [
        'js/min/main-custom-min.js',
    ];
}