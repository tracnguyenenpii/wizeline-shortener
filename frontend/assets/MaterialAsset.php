<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/13/16 3:15 PM
 */

namespace frontend\assets;

use enpii\enpiiCms\libs\NpAssetBundle;

class MaterialAsset extends NpAssetBundle
{
    public $sourcePath = '@enpii/enpiiCms/webassets/backend/style1/resource-files';
    public $css = [
        'global/css/components.min.css',
    ];

}