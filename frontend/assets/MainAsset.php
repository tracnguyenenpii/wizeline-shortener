<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/13/16 3:15 PM
 */

namespace frontend\assets;

use enpii\enpiiCms\libs\NpAssetBundle;

class MainAsset extends NpAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/fonts.css',
        'css/main.css',
    ];
    public $js = [
        'assets-enpii/libs/imagesloaded/imagesloaded.pkgd.min.js',
        'js/min/main-min.js',
    ];
}