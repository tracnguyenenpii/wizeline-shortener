<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShortenerItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shortener-item-search margin-bottom-10">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="input-group">

        <?= $form->field($model, 'globalSearch', [
            'options' => [
                'tag' => false,
            ],
        ])->textInput([
            'placeholder' => Yii::t(_NP_TEXT_CATE, 'Search...'),
            'class' => 'form-control'
        ])->label(false); ?>

        <span class="input-group-btn">
            <?= Html::submitButton(Yii::t(_NP_TEXT_CATE, 'Search') . ' <i class="m-icon-swapright m-icon-white"></i>', ['class' => 'btn green-haze']) ?>
        </span>
    </div>

    <?php ActiveForm::end(); ?>

</div>
