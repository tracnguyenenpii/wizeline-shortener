<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ShortenerItem */

$pageTitle = Yii::t('app', 'View {modelClass}', [
    'modelClass' => Yii::t('app', 'Shortener Item'),
]) . ' ID: ' . $model->id;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shortener Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>
<div class="container shortener-item-view">

    <h3 class="page-title"><?=  Html::encode(Yii::t('app', 'Shortener Item Management')) ?></h3>

    <div class="caption font-green-sharp bold uppercase">
        <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
    </div>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">'.
            Yii::t('app', 'Create Shortener Item'),
            ['create'],
            ['class' => 'btn blue btn-outline']) ?>
    </p>

    <?= Html::a(Url::base(true).Url::to(['shortener-item/redirect', 'slug' => $model->slug]), ['shortener-item/redirect', 'slug' => $model->slug], ['class' => 'btn btn-primary']) ?>



</div>
