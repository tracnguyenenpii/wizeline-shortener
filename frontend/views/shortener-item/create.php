<?php

use yii\helpers\Html;


/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ShortenerItem */

$pageTitle = Yii::t('app', 'Create Shortener Item');
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shortener Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageTitle;

?>
<div class="container shortener-item-create">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t('app', 'Shortener Item Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

    <h3>
        <?=  Html::encode(Yii::t('app', 'Latest Action')) ?>
    </h3>
    <?php
    /* @var $models \common\models\ShortenerItem[] */
    $models = \common\models\ShortenerItem::find()->orderBy('created_at DESC')->limit(10)->all();
    foreach ($models as $key => $model) {
        echo '<p class="shortener-item">';
        echo ($key+1). '. ' .Html::a(\yii\helpers\Url::base(true).\yii\helpers\Url::to(['shortener-item/redirect', 'slug' => $model->slug]), ['shortener-item/redirect', 'slug' => $model->slug]);
        echo '</p>';
    }
    ?>

</div>
