<?php

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ShortenerItem */
/* @var $form enpii\enpiiCms\libs\NpActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-green-sharp bold uppercase">
                    <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="shortener-item-form">

                    <?php $form = NpActiveForm::begin(['options' => ['class'=> 'single-form', 'role' => 'form']]); ?>

                    <div class="form-body">


                    <?= $form->field($model, 'original_url')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>



                    </div>

                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php NpActiveForm::end(); ?>

                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
