<?php

use yii\helpers\Html;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ShortenerItem */

$pageTitle = Yii::t('app', 'Update {modelClass}', [
    'modelClass' => 'Yii::t(\'app\', \'Shortener Item\')',
]) . ' ID: ' . $model->id;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shortener Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="shortener-item-update">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t('app', 'Shortener Item Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
