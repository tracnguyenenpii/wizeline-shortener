<?php
/* @var $this \enpii\enpiiCms\libs\NpView
 * @var $model \common\models\CouponPin;
 */
use yii\bootstrap\Html;
use common\helper\StringHelper;

$pageTitle = Yii::t('app', 'Home Page');

$this->setBrowserTitle($pageTitle);
$redirectUrl = Yii::$app->urlManager->createUrl(['site/thank-you']);
?>
    <div class="tmp-home">
        <div class="container">
            <div class="print-btn hide-item">
                <?= Html::button('PRINT / IMPRIMER',[
                    'class' => 'btn btn-default btn-print',
                ])?>
            </div>
            <img src="<?= Yii::$app->urlManager->createUrl(['offer/view-image', 'id' => StringHelper::base64UrlEncode($model->id)]) ?>"
                 alt="">
        </div>
        <?php if($model):?>
            <div id="force-print" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-wrapper">
                        <div class="modal-content">
                            <div class="print-btn">
                                <?= Html::button('PRINT / IMPRIMER',[
                                    'class' => 'btn btn-default btn-print',
                                ])?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php
$redirectUrl = Yii::$app->urlManager->createUrl(['site/thank-you',]);
$js = <<<JS
    window.sessionStorage.setItem('is_ta',1);
    jQuery(document).ready(function () {
        // jQuery wrapper
        (function ($) {
           $('#main-container').imagesLoaded( function() {
              $('.print-btn').removeClass('hide-item');
              $('.btn-print').click(function() {
                window.print(); 
                document.location.href = '{$redirectUrl}'
              })
            });
            var counter = 15;
    
            setInterval(function() {
                counter--;
                // Display 'counter' wherever you want to display it.
                if (counter === 0) {
                      $('#force-print').modal({
                           backdrop: 'static',
                           keyboard: false
                      });
                }
            
              }, 1000);
        })(jQuery);
    });
JS;
$this->registerJs($js);

