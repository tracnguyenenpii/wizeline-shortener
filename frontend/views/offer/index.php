<?php
/* @var $this \enpii\enpiiCms\libs\NpView
 */
use common\helper\StringHelper;
$pageTitle = Yii::t('app', 'Home Page');
$this->setBrowserTitle($pageTitle);
$couponPin = \common\models\CouponPin::getRandomCouponPinLN2();
?>
<div class="tmp-home">
    <div class="container">
    </div>

</div>
<?php
if($couponPin) {
    $redirectUrl = Yii::$app->urlManager->createUrl(['offer/save-pin','id' => StringHelper::base64UrlEncode($couponPin->id)]) ;
    $js = <<<JS
    jQuery(document).ready(function () {
        // jQuery wrapper
        (function ($) {
           
            var counter = 1;
    
            setInterval(function() {
                counter--;
     
                // Display 'counter' wherever you want to display it.
                if (counter === 0) {
                     window.location.href = "{$redirectUrl}";
                }
            
              }, 1000);
        })(jQuery);
    });
JS;
    $this->registerJs($js);
}


