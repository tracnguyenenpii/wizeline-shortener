<?php
/**
 * Created by PhpStorm.
 * User: lacphan
 * Date: 11/21/16
 * Time: 5:16 PM
 * @var $model \common\models\CouponPin;
 */
if($model) {
    header('Content-Type: image/png');

    $im = imagecreatefrompng(APP_IMAGE_PATH . '/Ticket_Alert_Coupon.png');

    $white = imagecolorallocate($im, 255, 255, 255);
    $grey = imagecolorallocate($im, 128, 128, 128);
    $black = imagecolorallocate($im, 0, 0, 0);

    $font = APP_FONT_PATH . '/Korolev-Bold.ttf';

    imagettftext($im, 10, 0, 35, 980, $black, $font, $model->pin);
    imagepng($im);
    imagedestroy($im);
}
