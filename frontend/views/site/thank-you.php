<?php
/* @var $this \enpii\enpiiCms\libs\NpView
 * @var $osrCategories \common\models\OsrCategory[]
 */
$pageTitle = Yii::t('app', 'Home Page');
$this->setBrowserTitle($pageTitle);
$couponPin = \common\models\CouponPin::getRandomCouponPinLN1();
?>
<div class="tmp-home tmp-thank-you">
    <div class="container">
        <div class="thank-you-content">
            <div id="thank-you-image">

            </div>
            <div id="thank-you-popup" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-wrapper">
                        <div class="modal-content">
                            <div class="thank-you-text">
                                <?= Yii::t('app','THANK YOU / MERCI')?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$imageUrl = Yii::$app->urlManager->baseUrl . '/images';
if($couponPin) {
    $js = <<<JS
    jQuery(document).ready(function () {
        // jQuery wrapper
        (function ($) {
            sessionStorage = window.sessionStorage;
            if(sessionStorage.getItem('is_et') || sessionStorage.getItem('is_ta')) {
                if(sessionStorage.getItem('is_et')) {
                $('#thank-you-image').html('<img src="{$imageUrl}/E-Ticket_Coupon.png" alt="E-Ticket_Coupon" width="1000" height="1000"/>');
                    sessionStorage.removeItem('is_et');             
                }
                if(sessionStorage.getItem('is_ta')) {
                     $('#thank-you-image').html('<img src="{$imageUrl}/Ticket_Alert_Coupon.png" alt="Ticket_Alert_Coupon" width="1000" height="1000"/>');
                     sessionStorage.removeItem('is_ta');
                }
                $('#thank-you-popup').modal({
                     backdrop: 'static',
                     keyboard: false
                 });
            }
           
          
        })(jQuery);
    });
JS;
    $this->registerJs($js);
}


