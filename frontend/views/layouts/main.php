<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/7/16 8:35 PM
 */

/* @var $this \enpii\enpiiCms\libs\NpView */
/* @var $content string */

use yii\helpers\Html;

\enpii\enpiiCms\webassets\fonts\assets\FontOpenSansAsset::register($this);
\enpii\enpiiCms\webassets\fonts\assets\FontAwesomeAsset::register($this);
\kartik\grid\ActionColumnAsset::register($this);
\enpii\enpiiCms\webassets\plugins\bootstrapcustom\assets\BootstrapCustomAsset::register($this);
\enpii\enpiiCms\webassets\plugins\colorbox\assets\ColorBoxAsset::register($this);
\enpii\enpiiCms\webassets\plugins\materialDesign\assets\MaterialDesignGeneralAsset::register($this);
\frontend\assets\MaterialAsset::register($this);
\frontend\assets\MainAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?></title>
    <meta name="description"
          content="Orville Coupons"/>

    <meta name="keywords"
          content="Orville Coupons"/>

    <meta name="robots" content="INDEX,FOLLOW"/>

    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Orville Coupons"/>
    <meta property="fb:admins" content="662484515"/>
    <meta property="og:title" content="Orville Coupons"/>
    <meta property="og:description"
          content="Orville Coupons"/>
    <meta property="og:url" content="<?= Yii::$app->urlManager->baseUrl?>"/>
    <meta property="og:image" content="<?= Yii::$app->urlManager->baseUrl . '/favicon-32x32.png'?> "/>

    <?php $this->head() ?>
</head>
<body class="<?= $this->getBodyClass() ?> page-header-fixed">
<div id="main-container" class="site-wrapper">
    <div class="site-inner">
        <?php $this->beginBody() ?>

        <?= \enpii\enpiiCms\widgets\BootstrapAlert::widget() ?>

        <?= $this->render('_header') ?>
        <div id="content" class="site-content">
            <?= $content ?>
        </div>
        <?= $this->render('_footer') ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
