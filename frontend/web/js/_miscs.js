/**
 * Created by npbtrac@yahoo.com on 8/16/16.
 */

NpApp.miscs = (function () {
    'use strict';

    return {
        sidebarMenuActive: function () {
            jQuery('#sidebar-primary-menu li li.active').each(function () {
                jQuery(this).parent().parent().addClass('open').addClass('active');
            });
        },
        init: function (inputOptions) {
            NpApp.miscs.sidebarMenuActive();

            jQuery('body').on('click', ".colorbox.iframe", function () {
                jQuery(this).colorbox({iframe: true, width: "80%", height: "80%", fixed: true});
            });

            jQuery('body').on('click', ".colorbox", function () {
                jQuery(this).colorbox();
            });
        }
    }
}());