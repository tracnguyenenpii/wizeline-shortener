/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

NpApp.alert = (function () {
    'use strict';

    var arrAlert = [];
    var options = {
        selector: '.np-alert',
        initTimeout: 3000,
        nextTimeout: 1000
    };

    return {
        next: function (i) {
            $('#' + arrAlert[i]).fadeToggle();
            if (typeof(arrAlert[i + 1]) != 'undefined') {
                setTimeout(function () {
                    NpApp.alert.next(i + 1);
                }, options.nextTimeout);
            }
        },
        init: function (inputOptions) {
            options = mergeOptions(options, inputOptions);

            var i = 0;
            $(options.selector).each(function () {
                arrAlert[i] = $(this).attr('id');
                i++;
            });

            setTimeout(function () {
                NpApp.alert.next(0);
            }, options.initTimeout);
        }
    }
}());