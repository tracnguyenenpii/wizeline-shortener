<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$arrConfigCommon = require(APP_BASE_PATH . '/common/config/main.php');
$arrConfigCommonLocal = require(APP_BASE_PATH . '/common/config/main-local.php');

$arrConfigLocal = require(__DIR__ . '/main-local.php');

$arrConfig = [
    'id' => 'app-frontend',
    'name' => 'WizeLine Shortener Test',

    // Assign base path of the frontend folder
    'basePath' => dirname(__DIR__),

    // Load components: log as bootstrap components
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        // Assign Frontend Url Manager to default Url Manager because this is Frontend
        'view' => [
            // Use a custom view class
            'class' => 'common\libs\ClsView',
        ],
        'urlManager' => $arrConfigCommon['components']['urlManagerFrontend'],

        'session' => [
            // Use a custom session class
            'class' => 'enpii\enpiiCms\libs\NpSession',
            'name' => '_frontendSessionId', // unique for Frontend
            'savePath' => '@root/frontend/runtime', // a temporary folder on frontend
        ],

        'assetManager' => [
            // Use a custom asset manager class
            'class' => 'enpii\enpiiCms\libs\NpAssetManager',
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/languages',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ]
            ]
        ],

        'user' => [
            'class' => 'common\libs\ClsWebUser',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'authTimeout' => 1800,
            'identityCookie' => [
                'name' => '_frontendUser', // unique for backend
                'path' => APP_BASE_URL  // correct path for the backend app.
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 10 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LddpCQTAAAAADkMcb59wigYVIq7n1Y9jKE4HCS3',
            'secret' => '6LddpCQTAAAAAPU27Z1X3nwsVnNed-9aDrk5moSA',
        ],
        'paypal' => [
            'class' => 'cinghie\paypal\components\Paypal',

        ],
    ],
    'params' => $params,
];
//echo '<pre>';
//print_r($arrConfig);
//echo '</pre>';
//die('ss');
return yii\helpers\ArrayHelper::merge(
    $arrConfigCommon,
    $arrConfigCommonLocal,
    $arrConfig,
    $arrConfigLocal
);


