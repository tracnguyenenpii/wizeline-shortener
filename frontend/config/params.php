<?php
return [
    'adminEmail' => 'noreplydevsrv@gmail.com',
    'priceOfOsrItem' => 20,
    'currency' => 'USD',
    'apiContextConfig' => [
        'mode' => 'sandbox',
        'http.ConnectionTimeOut' => 30,
        'http.Retry' => 1,
        'log.LogEnabled' => YII_DEBUG ? 1 : 0,
        'log.FileName' => Yii::getAlias('@frontend/runtime/logs/paypal.log'),
        'log.LogLevel' => 'ERROR',
        'validation.level' => 'log',
        'cache.enabled' => 'true'
    ]
];
