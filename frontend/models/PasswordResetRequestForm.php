<?php
namespace frontend\models;

use common\models\SystemNotification;
use common\models\User;
use yii\base\Model;
use yii\helpers\Html;
use yii;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::_STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::_STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        if ($user) {
            $user->generatePasswordResetToken();
            if ($user->save(false)) {
                $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_code]);
                $systemNotify = new SystemNotification();
                $systemNotify->name = 'Password reset';
                $systemNotify->content = '<div class="password-reset">
    <p>Hello ' . Html::encode($user->getFullName()) . ',</p>

    <p>Follow the link below to reset your password:</p>

    <p>' . Html::a(Html::encode($resetLink), $resetLink) . '</p>
</div>';
                $systemNotify->receiver_emails = $user->email;
                $systemNotify->is_emailed = 1;
                $systemNotify->is_smsed = 0;
                $systemNotify->save();
                return $systemNotify->sendEmail();
            }
        }

        return false;
    }
}
