<?php
namespace frontend\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;
use enpii\enpiiCms\helpers\HashHelper;

/**
 * Password reset form
 */
class ChangePasswordForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $rePassword;

    /**
     * @var \common\models\User
     */
    private $_user;

    public function __construct($config = [])
    {
        $this->_user = User::findOne(Yii::$app->user->id);
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'rePassword'], 'required'],
            ['oldPassword', 'validateOldPassword'],
            [['oldPassword', 'newPassword', 'rePassword'], 'string', 'min' => 4],
            ['rePassword', 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->newPassword);
            return $user->save(false);
        }
        return null;

    }

    public function validateOldPassword()
    {
        if (!$this->_user->validatePassword($this->oldPassword,null)) {
            $this->addError('oldPassword', 'Password is incorrect.');
        }
    }
}
