<?php
namespace frontend\models;

/**
 * Created by PhpStorm.
 * User: quocanhnguyen
 * Date: 8/13/16
 * Time: 5:27 PM
 */

use yii\base\Model;

class FormRegister extends Model
{
    public $firstName;
    public $lastName;
    public $nricNumber;
    public $mobileNumber;
    public $gender;
    public $dateOfBirth;
    public $nationality;
    public $centralRegion;
    public $eastRegion;
    public $northRegion;
    public $northEastRegion;
    public $westRegion;
    public $comfortable_dog;
    public $comfortable_cat;
    public $ironing;
    public $oven;
    public $refrigerator;
    public $windows;
    public $laundry;
    public $closet;
    public $spokenLanguage;
    public $otherTalent;
    public $timeslot_mon;
    public $timeslot_tue;
    public $timeslot_wed;
    public $timeslot_thu;
    public $timeslot_fri;
    public $timeslot_sat;
    public $timeslot_sun;
    public $_1st_referee_name;
    public $_1st_referee_contact;
    public $_2nd_referee_name;
    public $_2nd_referee_contact;
    public $email_address;
    public $account_password;
    public $confirm_password;

    public function rules()
    {
        return [

            [['firstName', 'lastName', 'nricNumber', 'mobileNumber'], 'required'],
            [['gender', 'dateOfBirth', 'nationality', 'centralRegion', 'eastRegion',
                'northRegion', 'northEastRegion', 'westRegion', 'comfortable_dog',
                'comfortable_cat', 'spokenLanguage', 'ironing', 'oven', 'refrigerator',
                'windows', 'laundry', 'closet', 'otherTalent', 'timeslot_8h'], 'safe'],


        ];
    }
}