<?php
/**
 * Created by PhpStorm.
 * User: quocanhnguyen
 * Date: 9/21/16
 * Time: 10:40 AM
 */

namespace frontend\models;


use common\models\Option;
use common\models\SystemNotification;
use enpii\enpiiCms\helpers\ArrayHelper;
use yii\base\Model;
use yii;

class ContactForm extends Model
{
    public $name;
    public $email;
    public $contactNumber;
    public $question;

    public function rules()
    {
        return [
            [['name', 'email', 'contactNumber', 'question'], 'required']
        ];
    }


    public function attributeLabels()
    {
        $parentOne = parent::attributeLabels();
        $thisOne = [

            'question' => Yii::t('app', 'Enquiry Details'),
        ];
        return ArrayHelper::merge($parentOne, $thisOne);
    }

    public function sendMail()
    {
        $strContent = ' A customer contacted with HAT. <br>
                        Contact information: <br>
                        Name: ' . $this->name . ' <br>
                        Email Address: ' . $this->email . '<br>
                        Contact Number: ' . $this->contactNumber . '<br>
                        Message: ' . $this->question;

        $systemNotify = new SystemNotification();
        $systemNotify->name = 'HAT Contact form';
        $systemNotify->content = $strContent;
        $systemNotify->receiver_emails = Option::get('admin_email');
        $systemNotify->is_emailed = 1;
        $systemNotify->is_smsed = 0;
        $systemNotify->email_template = 'systemNotificationDefaultMailTemplate';

        return $systemNotify->sendEmail();

    }

    public static function getLatLng($address)
    {

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');

        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);

        $json = curl_exec($curl);
        $arrayResult = json_decode($json);
        $result = $arrayResult->results[0]->geometry->location;
        return $result;
    }
}