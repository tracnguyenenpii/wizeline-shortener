<?php
namespace frontend\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;
use enpii\enpiiCms\helpers\HashHelper;

/**
 * @property $user common\models\User
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $password_confirm;

    /**
     * @var $_user \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {

        if (empty($token) || !is_string($token)) {

            Yii::$app->session->setFlash('error', 'Password reset token cannot be blank.');
            return null;
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            Yii::$app->session->setFlash('error', 'Wrong password reset token.');
            return null;
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'password_confirm'], 'required'],
            [
                ['password_confirm'],
                'compare',
                'compareAttribute' => 'password',
                'message' => Yii::t(_NP_TEXT_CATE, "Password doesn't match")
            ],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        if ($this->_user) {
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            $user->removePasswordResetToken();
            return $user->save(false);
        }
        return false;

    }

    /**
     * @return |null|static|common\models\User
     */
    public function getUser()
    {
        return $this->_user;
    }
}
