<?php
namespace frontend\models;


use enpii\enpiiCms\helpers\CommonHelper;
use yii\base\Model;
use yii;
use common\models\User;

/**
 * Signup form
 * @property $email;
 * @property $password;
 * @property User $_user
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['password', 'email'], 'required'],
            [['password'], 'validatePassword'],
            [['rememberMe'], 'validatePassword'],

        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password, null)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $session = Yii::$app->session;
            if ($session->has('loginFE')) {
                $session->remove('loginFE');
            }
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? (3600 * 24 * 7) : 0);
        } else {
            return false;
        }
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
