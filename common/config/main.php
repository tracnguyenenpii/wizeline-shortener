<?php
//use kartik\datecontrol\Module;

return [
    'vendorPath' => VENDOR_PATH,
    'language' => 'en',
    'timeZone' => 'UTC',
    'sourceLanguage' => 'en',
    'bootstrap' => ['enpiiCms'],
    'modules' => [
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => Yii::getAlias('@uploads'),
            'uploadUrl' => APP_BASE_URL.'/uploads',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],
        'enpiiCms' => [
            'class' => 'enpii\enpiiCms\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
            'downloadAction' => 'gridview/export/download',
        ],
        'dynamicrelations' => [
            'class' => '\synatree\dynamicrelations\Module'
        ],

//        'datecontrol' =>  [
//            'class' => 'kartik\datecontrol\Module',
//
//            // format settings for displaying each date attribute (ICU format example)
//            'displaySettings' => [
//                Module::FORMAT_DATE => 'dd-MM-yyyy',
//                Module::FORMAT_TIME => 'HH:mm:ss a',
//                Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a',
//            ],
//
//            // format settings for saving each date attribute (PHP format example)
//            'saveSettings' => [
//                Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
//                Module::FORMAT_TIME => 'php:H:i:s',
//                Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
//            ],
//
//            // set your display timezone
//            'displayTimezone' => 'Asia/Kolkata',
//
//            // set your timezone for date saved to db
//            'saveTimezone' => 'UTC',
//
//            // automatically use kartik\widgets for each of the above formats
//            'autoWidget' => true,
//
//            // use ajax conversion for processing dates from display format to save format.
//            'ajaxConversion' => true,
//
//            // default settings for each widget from kartik\widgets used when autoWidget is true
//            'autoWidgetSettings' => [
//                Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true]], // example
//                Module::FORMAT_DATETIME => [], // setup if needed
//                Module::FORMAT_TIME => [], // setup if needed
//            ],
//
//            // custom widget settings that will be used to render the date input instead of kartik\widgets,
//            // this will be used when autoWidget is set to false at module or widget level.
//            'widgetSettings' => [
//                Module::FORMAT_DATE => [
//                    'class' => 'yii\jui\DatePicker', // example
//                    'options' => [
//                        'dateFormat' => 'php:d-M-Y',
//                        'options' => ['class'=>'form-control'],
//                    ]
//                ]
//            ]
//            // other settings
//        ]
    ],
    'components' => [
        // URL Manager for backend
        'urlManagerBackend' => [
            'class' => 'common\libs\ClsUrlManager',
            // we don't need pretty url here
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => APP_BASE_URL . "/admin",
            'languages' => ['en', 'vi'],
            'enableDefaultLanguageUrlCode' => false,
            'enableLanguageDetection' => false,
            'enableLanguagePersistence' => false,
            'rules' => [

            ]
        ],

        // URL Manager for frontend
        'urlManagerFrontend' => [
            'class' => 'common\libs\ClsUrlManager',
            // enable pretty URL here
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => APP_BASE_URL . "",
            'languages' => ['en', 'vi'],
            'enableDefaultLanguageUrlCode' => false,
            'enableLanguageDetection' => false,
            'enableLanguagePersistence' => false,
            'rules' => [
                'shorten' => 'shortener-item/redirect',

            ]
        ],

        // URL Manager for backend
        'urlManagerApi' => [
            'class' => 'common\libs\ClsUrlManager',
            // we don't need pretty url here
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => APP_BASE_URL . "/ws",

            // only english in API
            'languages' => ['en'],
            'enableDefaultLanguageUrlCode' => false,
            'enableLanguageDetection' => false,
            'enableLanguagePersistence' => false,
            'rules' => [

            ]
        ],

        'assetManager' => [
            // Use a custom asset manager class
            'class' => 'enpii\enpiiCms\libs\NpAssetManager',
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'common\libs\ClsPhpMessageSource',
                    'basePath' => '@app/languages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'enpii*' => [
                    // Set translation using .php file
                    'class' => 'enpii\enpiiCms\libs\NpPhpMessageSource',
                    'basePath' => '@enpii/enpiiCms/languages',
                    'fileMap' => [
                        'enpii' => 'enpii.php',
                    ],
                ]
            ]
        ],

        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],

        // Caching system
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        // RBAC
        'authManager' => [
            'class' => 'enpii\enpiiCms\libs\NpAuthDbManager',
        ],

        // Some format
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
            'timeZone' => 'America/Toronto'
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'viewPath' => '@common/mail',
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.mandrillapp.com',
//                'username' => 'VN',
//                'password' => 'iFP5vBIRJC8Y243ZhjJCqA',
//                'port' => '587'
//            ],
        ],
    ],
];