<?php
// Defining root namespace to the root folder of application
Yii::setAlias('root', dirname(dirname(__DIR__)));

// Defining common namespace
Yii::setAlias('common', dirname(__DIR__));

Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
// Defining root namespace to the uploads folder of application
Yii::setAlias('uploads', dirname(dirname(__DIR__)) . '/uploads');
