<?php
return [
    'default' => [
        'dateFormat' => 'm-d-Y',
        'datetimeFormat' => 'm-d-Y H:i:s',
        'timeFormat' => 'H:i:A',
        'timezone' => 'Asia/Ho_Chi_Minh', //global date formats for display for your locale.
    ],
];
