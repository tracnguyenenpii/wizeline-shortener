<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/5/16 12:27 AM
 */

namespace common\libs;

use common\models\User;
use enpii\enpiiCms\libs\NpControllerBackend;
use Yii;

class ClsControllerBackend extends NpControllerBackend
{
    public function init()
    {
        $this->layout = '@enpii/enpiiCms/views/layouts/backend/main';
        Yii::$app->view->params['_sidebar_path'] = "@app/views/layouts/_sidebar";
        parent::init();
    }
}