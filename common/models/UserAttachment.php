<?php
namespace common\models;

use enpii\enpiiCms\helpers\ArrayHelper;
use enpii\enpiiCms\libs\behaviors\NpSluggableBehavior;

class UserAttachment extends \enpii\enpiiCms\models\UserAttachment
{
    public function behaviors()
    {
        $parentOneConfigs = parent::behaviors();
        $thisOneConfigs = [
            [
                'class' => NpSluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'code',
                'ensureUnique' => true,
                'limit' => 30,
            ],
            [
                'class' => NpSluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
            ],

        ];

        return ArrayHelper::merge($parentOneConfigs, $thisOneConfigs);
    }
}
