<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShortenerItem;
use enpii\enpiiCms\libs\NpActiveQuery;

/**
 * ShortenerItemSearch represents the model behind the search form about `common\models\ShortenerItem`.
 * @property  $globalSearch
 */
class ShortenerItemSearch extends ShortenerItem
{
    /**
    * @var $globalSearch
    */
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['slug', 'original_url', 'created_at', 'updated_at'], 'safe'],
            [['globalSearch'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $deleted = 0)
    {
        $query = ShortenerItem::find();

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'original_url', $this->original_url]);
        if($this->globalSearch) {
        $query->orFilterWhere(['like','id', $this->globalSearch]);
        $query->orFilterWhere(['like','slug', $this->globalSearch]);
        $query->orFilterWhere(['like','original_url', $this->globalSearch]);
        $query->orFilterWhere(['like','created_at', $this->globalSearch]);
        $query->orFilterWhere(['like','updated_at', $this->globalSearch]);
                }

        // Return things not in trash it $deleted not set
        if (!$deleted) {
            $query = $query->notDeleted();
        }
        return $dataProvider;
    }
}
