<?php
namespace common\models;

use common\models\base\BaseUserTechnicianPreferredLocation;
use enpii\enpiiCms\libs\behaviors\NpSluggableBehavior;
use enpii\enpiiCms\libs\behaviors\NpTimestampBehavior;
use yii\helpers\ArrayHelper;
use yii;

class UserTechnicianPreferredLocation extends BaseUserTechnicianPreferredLocation
{
    public $state_ids;

    public function rules()
    {
        $parentConfigs = parent::rules();
        $thisConfigs = [
            [['state_ids'], 'safe'],
        ];
        return ArrayHelper::merge($parentConfigs, $thisConfigs);
    }

    public function getStepValidationFields($step)
    {
        switch ($step) {
            default:
                return ['state_ids'];
                break;
        }
    }

    public function saveStates($user_id)
    {
        if (!is_array($this->state_ids)) {
            $this->state_ids = [$this->state_ids];
        }

        $boolResult = true;
        UserTechnicianPreferredLocation::deleteAll(['user_id' => $user_id]);
        foreach ($this->state_ids as $key => $state_id) {
            $locationPreferred = new UserTechnicianPreferredLocation();
            $locationPreferred->user_id = $user_id;
            $locationPreferred->state_id = $state_id;

            if (!$locationPreferred->save()) {
                $boolResult = false;
            }
        }
        return $boolResult;
    }

    public function getLocationPreferred($user_id)
    {
        $query = UserTechnicianPreferredLocation::find()->filterWhere([
            '=',
            'user_id',
            $user_id
        ]);
        if (!is_array($this->state_ids)) {
            $this->state_ids = [];
        }
        $arr = $query->all();
        if ($arr) {
            foreach ($arr as $item) {
                $this->state_ids[] = $item['state_id'];
            }
        }
    }

    public function deleteAllByUserID($user_id)
    {

    }
}
