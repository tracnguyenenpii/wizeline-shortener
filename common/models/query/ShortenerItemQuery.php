<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ShortenerItem]].
 *
 * @see \common\models\ShortenerItem
 */
class ShortenerItemQuery extends \common\libs\ClsActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\ShortenerItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ShortenerItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}