<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\UserPermissionUserRoleRelation]].
 *
 * @see \common\models\UserPermissionUserRoleRelation
 */
class UserPermissionUserRoleRelationQuery extends \enpii\enpiiCms\models\query\UserPermissionUserRoleRelationQuery
{

}