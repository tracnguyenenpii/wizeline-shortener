<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\CouponPin]].
 *
 * @see \common\models\CouponPin
 */
class CouponPinQuery extends \enpii\enpiiCms\libs\NpActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\CouponPin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\CouponPin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}