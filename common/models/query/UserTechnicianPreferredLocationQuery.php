<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\enpii\enpiiCms\models\base\UserTechnicianPreferredLocation]].
 *
 * @see \common\models\UserTechnicianPreferredLocation
 */
class UserTechnicianPreferredLocationQuery extends \common\libs\ClsActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\UserTechnicianPreferredLocation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\UserTechnicianPreferredLocation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}