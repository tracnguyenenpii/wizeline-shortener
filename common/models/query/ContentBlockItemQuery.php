<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ContentBlockItem]].
 *
 * @see \common\models\ContentBlockItem
 */
class ContentBlockItemQuery extends \common\libs\ClsActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\ContentBlockItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ContentBlockItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}