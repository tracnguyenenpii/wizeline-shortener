<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\enpii\enpiiCms\models\base\UserProfileTechnician]].
 *
 * @see \common\models\base\UserProfileTechnician
 */
class UserProfileTechnicianQuery extends \common\libs\ClsActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\UserProfileTechnician[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\UserProfileTechnician|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}