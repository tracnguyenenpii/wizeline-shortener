<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CouponPin;
use enpii\enpiiCms\libs\NpActiveQuery;

/**
 * CouponPinSearch represents the model behind the search form about `common\models\CouponPin`.
 * @property  $globalSearch
 */
class CouponPinSearch extends CouponPin
{
    /**
    * @var $globalSearch
    */
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_obtained'], 'integer'],
            [['pin', 'http_referrer', 'user_browser_agent', 'user_email', 'user_ip', 'user_session', 'notes', 'obtained_at'], 'safe'],
            [['globalSearch'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $deleted = 0)
    {
        $query = CouponPin::find();

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'obtained_at' => $this->obtained_at,
            'is_obtained' => $this->is_obtained,
        ]);

        $query->andFilterWhere(['like', 'pin', $this->pin])
            ->andFilterWhere(['like', 'http_referrer', $this->http_referrer])
            ->andFilterWhere(['like', 'user_browser_agent', $this->user_browser_agent])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'user_ip', $this->user_ip])
            ->andFilterWhere(['like', 'user_session', $this->user_session])
            ->andFilterWhere(['like', 'notes', $this->notes]);
        if($this->globalSearch) {
        $query->orFilterWhere(['like','id', $this->globalSearch]);
        $query->orFilterWhere(['like','pin', $this->globalSearch]);
        $query->orFilterWhere(['like','http_referrer', $this->globalSearch]);
        $query->orFilterWhere(['like','user_browser_agent', $this->globalSearch]);
        $query->orFilterWhere(['like','user_email', $this->globalSearch]);
        $query->orFilterWhere(['like','user_ip', $this->globalSearch]);
        $query->orFilterWhere(['like','user_session', $this->globalSearch]);
        $query->orFilterWhere(['like','notes', $this->globalSearch]);
        $query->orFilterWhere(['like','obtained_at', $this->globalSearch]);
        $query->orFilterWhere(['like','is_obtained', $this->globalSearch]);
                }

        return $dataProvider;
    }
}
