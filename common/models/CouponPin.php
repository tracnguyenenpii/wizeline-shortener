<?php
namespace common\models;

use common\models\base\BaseCouponPin;
use enpii\enpiiCms\helpers\ArrayHelper;
use enpii\enpiiCms\libs\behaviors\NpTimestampBehavior;

class CouponPin extends BaseCouponPin{

    public function behaviors()
    {
        $parentOneConfigs = parent::behaviors();
        $thisOneConfigs = [
            [
                'class' => NpTimestampBehavior::className(),
                'updatedAtAttribute' => 'obtained_at',
            ],
        ];

        return ArrayHelper::merge($parentOneConfigs, $thisOneConfigs);
    }

    public static function isObtainedCoupon() {
        $couponPin = CouponPin::find()->where(['user_ip' => $_SERVER['REMOTE_ADDR']])->one();
        if($couponPin) {
            return true;
        }
        return false;
    }

    public function prepareCouponPin() {
        $this->user_ip = $_SERVER['REMOTE_ADDR'];
        $this->user_browser_agent = $_SERVER['HTTP_USER_AGENT'];
        $this->is_obtained = 1;
    }

    public static function getRandomCouponPinLN1() {
        return CouponPin::find()->where(['is_obtained' => 0])->andFilterWhere(['like', 'pin' , 'LN1'])->orderBy(['rand()' => SORT_DESC])->one();
    }

    public static function getRandomCouponPinLN2() {
        return CouponPin::find()->where(['is_obtained' => 0])->andFilterWhere(['like', 'pin' , 'LN2'])->orderBy(['rand()' => SORT_DESC])->one();
    }
}
