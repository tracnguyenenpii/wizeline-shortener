<?php
namespace common\models;

use common\models\base\BaseContentBlockItem;
use yii\helpers\ArrayHelper;
use enpii\enpiiCms\libs\behaviors\NpSluggableBehavior;
use enpii\enpiiCms\libs\behaviors\NpTimestampBehavior;

class ContentBlockItem extends BaseContentBlockItem
{
    public function behaviors()
    {
        $parentOneConfigs = parent::behaviors();
        $thisOneConfigs = [
            [
                'class' => NpSluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
            ],
            [
                'class' => NpSluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'code',
                'ensureUnique' => true,
                'limit' => 28,
            ],
            [
                'class' => NpTimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];

        return ArrayHelper::merge($parentOneConfigs, $thisOneConfigs);
    }

    /**
     * Get content of a block item by code
     * @param $code
     * @return bool|string
     */
    public static function getContentByCode($code)
    {
        $model = ContentBlockItem::findOne(['code' => $code]);
        return empty($model) ? false : $model->content;
    }
}
