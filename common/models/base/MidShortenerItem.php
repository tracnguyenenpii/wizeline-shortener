<?php

namespace common\models\base;

use Yii;

if (class_exists("\\common\\models\\base\\BaseShortenerItem")) {
    class MidShortenerItem extends \common\models\base\BaseShortenerItem    {

    }
} else {
    class MidShortenerItem extends BaseShortenerItem    {

    }
}
