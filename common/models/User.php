<?php

namespace common\models;

use common\helper\NotifyMessage;
use common\models\base\BaseUser;
use enpii\enpiiCms\components\validators\UsernameValidator;
use enpii\enpiiCms\helpers\ArrayHelper;
use yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/*
 * @property string $experience
 */

class User extends \enpii\enpiiCms\models\User
{
    const _SCENARIO_CREATE = 'create';
    const _SCENARIO_CREATE_TECHNICIAN = 'create_technician';
    const _SCENARIO_UPDATE = 'update';
    const _SCENARIO_CHANGE_PASS = 'change_pass';
    const _STATUS_PENDING = 0;
    const _STATUS_ACTIVE = 1;
    const _STATUS_DENIED = 2;
    const _STANDARD_ID = 4;
    const _TECH_EXTERNAL_ID = 5;
    const _TECH_INTERNAL = 6;
    const _TECH_INTERNAL_CODE = 'tech-internal';
    const _TECH_EXTERNAL_CODE = 'tech-external';
    const _STANDARD_CODE = 'standard';

    public $password;
    public $password_confirm;
    public $preferred_state_ids;
    public $sameAsBillingAddress = false;
    public $technician_category_ids;
    public $technician_service_type_ids;


    public function getStepValidationFields($step)
    {
        switch ($step) {
            case 2:
                return ['email', 'password', 'password_confirm'];
                break;
            default:
                return ['first_name', 'last_name', 'mobile_phone', 'work_permit_number'];
                break;
        }
    }

    public function getBookingStepValidationFields($step)
    {
        switch ($step) {
            case 1:
                return ['first_name', 'last_name', 'mobile_phone', 'billing_address1', 'billing_post_code', 'billing_city'];
                break;
            case 2:
                return ['email', 'password', 'password_confirm'];
                break;
            default:
                return [];
                break;
        }

    }

    public function rules()
    {
        return [
            [['username', 'email', 'first_name', 'last_name'], 'required'],
            [['password'], 'required', 'on' => static::_SCENARIO_CREATE],
            [
                ['password_confirm'],
                'required',
                'when' => function ($model) {
                    return !empty($model->password);
                },
                'whenClient' => "function (attribute, value) {
                return ($('#" . "user-password" . "').val());
            }"
            ],
            [
                ['password_confirm'],
                'compare',
                'compareAttribute' => 'password',
                'message' => Yii::t(_NP_TEXT_CATE, "Password doesn't match")
            ],

            // A custom validator for username
            [['username'], UsernameValidator::className()],
            [['email'], 'validateEmail', 'on' => static::_SCENARIO_CREATE],
//            ['email', 'unique', 'targetAttribute' => 'email'],
            [['profile_id', 'parent_id', 'level', 'creator_id', 'is_deleted', 'ordering_weight', 'status'], 'integer'],
            [['created_at', 'updated_at', 'published_at'], 'safe'],
            [['params'], 'string'],
            [['username', 'api_key', 'api_secret'], 'string', 'max' => 32],
            [
                ['email', 'first_name', 'middle_name', 'last_name', 'password_hash', 'password_salt'],
                'string',
                'max' => 255
            ],
            [['phone'], 'string', 'max' => 16],
            [['password_reset_code', 'auth_code'], 'string', 'max' => 8],
            [['billing_address', 'billing_address2', 'service_address', 'service_address2'], 'string'],


            [['billing_address', 'service_address'], 'required', 'on' => static::_SCENARIO_UPDATE],

            [['user_role_ids'], 'required'],

            // Declare safe for controller->load function to load the value
            [['password', 'password_confirm', 'user_role_ids', 'avatar'], 'safe'],

            [['billing_post_code', 'service_post_code', 'postal_code'], 'integer'],
            [['billing_post_code', 'service_post_code', 'postal_code'], 'string', 'max' => 6, 'min' => 6],
            [['work_permit_number'], 'string', 'min' => 9, 'tooShort' => Yii::t(_NP_TEXT_CATE, 'NRIC has to be minimum 9 characters.')],
            [['mobile_phone', 'billing_address', 'service_address', 'postal_code', 'billing_post_code', 'billing_country', 'service_post_code', 'service_country', 'postal_code', 'billing_city', 'service_city', 'countryText'], 'required'],

            [['mobile_phone', 'address', 'state', 'preferred_state_ids', 'sameAsBillingAddress', 'technician_category_ids', 'technician_service_type_ids', 'countryText'], 'safe'],

            [['mobile_phone', 'address', 'state', 'preferred_state_ids', 'sameAsBillingAddress', 'qualification_level', 'qualification_type', 'experience'], 'safe'],
            ['email', 'email'],
            [['work_permit_number'], 'required', 'on' => static::_SCENARIO_CREATE_TECHNICIAN]

        ];


    }

    public function attributeLabels()
    {
        $parentOne = parent::attributeLabels();
        $thisOne = [
            'last_name' => Yii::t(_NP_TEXT_CATE, 'Last Name (\'Surname\')'),
            'work_permit_number' => Yii::t(_NP_TEXT_CATE, 'NRIC'),
        ];

        return ArrayHelper::merge($parentOne, $thisOne);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::_STATUS_ACTIVE, 'is_deleted' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::_STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::_STATUS_ACTIVE, 'is_deleted' => 0]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {

        return static::findOne([
            'password_reset_code' => $token,
            'status' => self::_STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_code;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_code = Yii::$app->security->generateRandomString(8);
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_code = null;
    }

    public static function hasLoginBE()
    {
        $hasLogin = Yii::$app->session->setName('_backendID');
        return $hasLogin;
    }
//    public function getProfile()
//    {
//        return $this->hasOne(CommonUserProfile::className(), ['id' => 'profile_id']);
//    }

    public static function validateUserNameIsDeleted($username)
    {
        $user = self::findByUsername($username);

        if ($user && $user->is_deleted == 1) {
            return true;
        }
        return false;
    }

    public function sendActivateCode()
    {
        $activateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-user', 'email' => $this->email, 'token' => $this->auth_code]);
//
//        $strContent = NotifyMessage::getUserRegisterToUser([
//            'activeLink' => $activateLink,
//            'userName' => $this->getFullName()
//        ]);
        if (Option::get('user_validation_enable' == 1)) {
            $email_template = EmailTemplate::getTemplateByCode('consumer-register');

        } else {
            $email_template = EmailTemplate::getTemplateByCode('template-consumer-signup-not-active');

        }
        $strContent = $email_template->content;
        $strContent = str_replace('{user_name}', $this->getFullName(), $strContent);
        $strContent = str_replace('{activate_link}', $activateLink, $strContent);

        $systemNotify = new SystemNotification();
        $systemNotify->name = 'User register from HAT';
        $systemNotify->content = $strContent;
        $systemNotify->receiver_emails = $this->email;
        $systemNotify->is_emailed = 1;
        $systemNotify->is_smsed = 0;
        $systemNotify->template_id = $email_template->id;
        $systemNotify->save();
        return $systemNotify->sendEmail();

    }

    public function generateAuthCode()
    {
        $this->auth_code = Yii::$app->security->generateRandomString(8);
    }

    public static function getSelect2Data($items, $useType = false, $remote = true)
    {
        if (empty($items)) {
            return false;
        }
        if (!is_array($items)) {
            $items = [$items];
        }

        $rows = [];
        foreach ($items as $index => $item) {
            if (!($item instanceof yii\db\ActiveRecord)) {
                // array of id
                $item = static::findByID($item);
            }

            $row = [];
            $row['id'] = $item->id;
            $row['text'] = $item->username;
            $row['type'] = Yii::t('app', 'Available Countries');
            if ($useType && $remote) {
                $boolExists = 0;
                foreach ($rows as $key => $value) {
                    if (!empty($value['text']) && $value['text'] == $row['type']) {
                        if (empty($value['children'])) {
                            $value['children'] = [];
                        }
                        $rows[$key]['children'][] = $row;
                        $boolExists = true;
                    }
                }

                if (!$boolExists) {
                    $rows = ArrayHelper::merge($rows, [['text' => $row['type'], 'children' => [$row]]]);
                }
            } else {
                $rows[] = $row;
            }
        }

        if (!$remote) {
            return $useType ? ArrayHelper::map($rows, 'id', 'text', 'type') : ArrayHelper::map($rows, 'id', 'text');
        }

        return $rows;

    }

    public static function getUserNotAssign()
    {
        $user = User::find()
            ->join('LEFT OUTER JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.technician_id')
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user}}.id = {{%user_user_role_relation}}.user_id')
            ->where('{{%osr_item}}.technician_id IS NULL')
            ->andWhere('{{%user_user_role_relation}}.user_role_id=' . self::_TECH_EXTERNAL_ID)
            ->limit(20)->all();
        return $user;
    }

    /**
     * @param $roles string
     * @return bool
     */

    public function userCan($roles)
    {
        $userRoles = $this->getUserRoles();
        $arrayUserRoles = ArrayHelper::map($userRoles, 'id', 'code');
        if (in_array($roles, $arrayUserRoles)) {
            return true;
        }
        return false;

    }

    public static function getStatuses()
    {
        return [
            static::_STATUS_DISABLED => Yii::t(_NP_TEXT_CATE, 'Pending'),
            static::_STATUS_ACTIVE => Yii::t(_NP_TEXT_CATE, 'Active'),
            static::_STATUS_DENIED => Yii::t(_NP_TEXT_CATE, 'Deactive'),
        ];
    }

    /**
     * Active user , set status = 1
     * @return bool
     */
    public function setActiveUser()
    {
        if ($this->status != self::_STATUS_ACTIVE) {
            $this->status = self::_STATUS_ACTIVE;
            return $this->save(true, array('status'));
        } else {
            return false;
        }
    }

    /**
     * Denied user , set status = 2
     * @return bool
     */
    public function setDenyUser()
    {
        if ($this->status != self::_STATUS_DENIED) {
            $this->status = self::_STATUS_DENIED;
            return $this->save(true, array('status'));
        } else {
            return false;
        }
    }

    public function preparePreferredStates()
    {
        $this->preferred_state_ids = $this->getPreferredStates();
    }

    public function getPreferredStates()
    {
        if ($this->id) {
            /* @var @query UserUserRoleRelationQuery */
            $query = UserTechnicianPreferredLocation::find();
            $query->select('state_id')->andFilterWhere(['user_id' => $this->id]);
            foreach ($arrResult = $query->asArray()->all() as $key => $value) {
                $arrResult[$key] = $value['state_id'];
            }
        } else {
            $arrResult = [];
        }

        return $arrResult;
    }

    public function savePreferredStates()
    {
        if (!is_array($this->preferred_state_ids)) {
            $this->preferred_state_ids = [$this->preferred_state_ids];
        }

        $boolResult = false;
        UserTechnicianPreferredLocation::deleteAll(['user_id' => $this->id]);
        foreach ($this->preferred_state_ids as $key => $preferred_state_id) {
            $preferredStates = new UserTechnicianPreferredLocation();
            $preferredStates->user_id = $this->id;
            $preferredStates->state_id = $preferred_state_id;
            if ($preferredStates->save()) {
                $boolResult = true;
            }
        }
        return $boolResult;
    }

    /*
     * Check a user has role
     * @param $role string, code of role
     * @return bool
     */
    public static function hasRole($role)
    {
        $roles = User::findOne(Yii::$app->user->id)->getUserRoles();
        $role_arr = ArrayHelper::map($roles, 'id', 'code');

        if (in_array($role, $role_arr)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Count number technician External are pending
     * @return int
     */
    public static function getTechnicianInternal()
    {
        $user = User::find()
            ->join('LEFT OUTER JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.technician_id')
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user}}.id = {{%user_user_role_relation}}.user_id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id')
            ->where('{{%osr_item}}.technician_id IS NULL')
            ->andWhere('{{%user_role}}.code="' . self::_TECH_INTERNAL_CODE . '"')
            ->andFilterWhere(['{{%user}}.is_deleted' => 0])
            ->limit(20)->all();

        return $user;
    }

    /**
     * @return array| OsrUserReview[]
     * Function get all review of standard user
     */
    public function getUserReview()
    {
        return OsrUserReview::find()
            ->join('LEFT JOIN', '{{%osr_item}}', '{{%osr_item}}.id = {{%osr_user_review}}.osr_item_id AND {{%osr_item}}.technician_id = {{%osr_user_review}}.technician_id')
            ->andFilterWhere(['{{%osr_item}}.creator_id' => $this->id])->all();
    }

    /**
     * @return float|int|null
     * Function for get average total review
     */
    public function getUserReviewScore()
    {
        if ($this->userCan('standard')) {
            $reviews = $this->getUserReview();
        } else {
            $reviews = $this->getTechnicianReview();
        }
        $index = 0;
        $score = 0;
        if (is_array($reviews)) {
            foreach ($reviews as $key => $review) {
                $index++;
                $score += $review->score;
            }
        }
        if ($index > 0) {
            return $score / $index;
        }
        return null;
    }


    /**
     * @return array| OsrUserReview[]
     * Function fo get all review of technician
     */
    public function getTechnicianReview()
    {
        return OsrUserReview::find()
            ->join('LEFT JOIN', '{{%osr_item}}', '{{%osr_item}}.id = {{%osr_user_review}}.osr_item_id AND {{%osr_item}}.creator_id = {{%osr_user_review}}.user_id')
            ->andFilterWhere(['{{%osr_item}}.technician_id' => $this->id])
            ->all();
    }

    public static function countTechnicianExternalPending()
    {
        $result = User::find()
            ->join('LEFT OUTER JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.technician_id')
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user}}.id = {{%user_user_role_relation}}.user_id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id')
            ->where('{{%osr_item}}.technician_id IS NULL')
            ->andWhere('{{%user_role}}.code="' . self::_TECH_EXTERNAL_CODE . '"')
            ->andWhere('{{%user}}.status=' . self::_STATUS_PENDING)
            ->andFilterWhere(['{{%user}}.is_deleted' => 0])->count();

        return $result;
    }

    /*
     * Count total technician Internal in the system
     * @return int
     */
    public static function countTotalTechnicianInternal()
    {
        $result = User::find()
            ->join('LEFT OUTER JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.technician_id')
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user}}.id = {{%user_user_role_relation}}.user_id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id')
            ->where('{{%osr_item}}.technician_id IS NULL')
            ->andWhere('{{%user_role}}.code="' . self::_TECH_INTERNAL_CODE . '"')
            ->andFilterWhere(['{{%user}}.is_deleted' => 0])
            ->limit(20)->count();

        return $result;
    }

    public function prepareTechnicianCategory()
    {
        $this->technician_category_ids = $this->getCategoryIds();
    }

    public function getCategoryIds()
    {
        if ($this->id) {
            /* @var @query TechnicianCategoryRelationQuery */
            $query = TechnicianCategoryRelation::find();
            $query->select('osr_category_id')->andFilterWhere(['technician_id' => $this->id]);
            foreach ($arrResult = $query->asArray()->all() as $key => $value) {
                $arrResult[$key] = $value['osr_category_id'];
            }
        } else {
            $arrResult = [];
        }

        return $arrResult;
    }

    public function saveTechnicianCategory()
    {
        if (!is_array($this->technician_category_ids)) {
            $this->technician_category_ids = [$this->technician_category_ids];
        }

        $boolResult = true;
        TechnicianCategoryRelation::deleteAll(['technician_id' => $this->id]);
        foreach ($this->technician_category_ids as $key => $category_id) {
            $modelTechnicianCategoryRoleRelation = new TechnicianCategoryRelation();
            $modelTechnicianCategoryRoleRelation->technician_id = $this->id;
            $modelTechnicianCategoryRoleRelation->osr_category_id = $category_id;
            if (!$modelTechnicianCategoryRoleRelation->save()) {
                $boolResult = false;
            }
        }
        return $boolResult;
    }

    /*
     * service Type
     */
    public function prepareTechnicianServiceType()
    {
        $this->technician_service_type_ids = $this->getServiceTypeIds();
    }

    public function getServiceTypeIds()
    {
        if ($this->id) {
            /* @var @query TechnicianServiceTypeRelationQuery */
            $query = TechnicianServiceTypeRelation::find();
            $query->select('service_type_id')->andFilterWhere(['technician_id' => $this->id]);
            foreach ($arrResult = $query->asArray()->all() as $key => $value) {
                $arrResult[$key] = $value['service_type_id'];
            }
        } else {
            $arrResult = [];
        }

        return $arrResult;
    }

    public function saveTechnicianServiceType()
    {
        if (!is_array($this->technician_service_type_ids)) {
            $this->technician_service_type_ids = [$this->technician_service_type_ids];
        }

        $boolResult = true;
        TechnicianServiceTypeRelation::deleteAll(['technician_id' => $this->id]);
        foreach ($this->technician_service_type_ids as $key => $service_type_id) {
            $modelTechnicianServiceTypeRoleRelation = new TechnicianServiceTypeRelation();
            $modelTechnicianServiceTypeRoleRelation->technician_id = $this->id;
            $modelTechnicianServiceTypeRoleRelation->service_type_id = $service_type_id;
            if (!$modelTechnicianServiceTypeRoleRelation->save()) {
                $boolResult = false;
            }
        }
        return $boolResult;
    }

    public function getCountryText()
    {
        return LocationCountry::findOne($this->billing_country);
    }

    public function getCityText()
    {
        return LocationState::findOne($this->billing_city);
    }

    public function getServiceCountryText()
    {
        return LocationCountry::findOne($this->service_country);
    }

    public function getServiceCityText()
    {
        return LocationState::findOne($this->service_city);
    }

    public static function getQualificationLevels()
    {
        return [
            'Certificate' => 'Certificate',
            'Diploma' => 'Diploma',
            'Bachelor Degree' => 'Bachelor Degree',
            'Honours Degree' => 'Honours Degree',
            'Master' => 'Master',
        ];
    }

    public function getCategoryText()
    {
        $relation = TechnicianCategoryRelation::findOne(['technician_id' => $this->id]);
        if ($relation) {
            $category = OsrCategory::findOne($relation->osr_category_id);
            if ($category) {
                return $category->name;
            }
        }
        return null;
    }

    public function getServiceTypeText()
    {
        $relation = TechnicianServiceTypeRelation::findOne(['technician_id' => $this->id]);
        if ($relation) {
            $result = OsrServiceType::findOne($relation->service_type_id);
            if ($result) {
                return $result->name;
            }
        }
        return null;
    }

    public function getPreferredLocationText()
    {
        $relation = UserTechnicianPreferredLocation::find()->filterWhere([
            '=',
            'user_id',
            $this->id
        ])->all();
        if ($relation) {
            $result = '';
            foreach ($relation as $item) {
                $temp = LocationState::findOne($item->state_id);
                if ($temp) {
                    $result .= $temp->name . ', ';
                }
            }
            return $result;
        }
        return null;
    }

    public function validateEmail()
    {
        $user = User::findByUsername($this->email);
        if ($user && $user->is_deleted == 0) {
            $this->addError('email', 'Email already registered.');
        }
    }

    public function delete()
    {
        UserUserRoleRelation::findOne(['user_id' => $this->id])->delete();
        return parent::delete(); // TODO: Change the autogenerated stub
    }
}
