<?php
namespace common\models;

use common\models\base\MidShortenerItem;

class ShortenerItem extends MidShortenerItem{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['original_url'], 'string'],
            [['original_url'], 'required'],
            [['original_url'], 'url'],
            [['slug'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
            [['slug'], 'string', 'max' => 255]
        ];
    }
}
