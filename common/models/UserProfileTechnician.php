<?php
namespace common\models;

use common\models\base\BaseUserProfileTechnician;

class UserProfileTechnician extends BaseUserProfileTechnician
{
    public function getStepValidationFields($step)
    {
        switch ($step) {
            case 1:
                return ['ok_with_dogs', 'ok_with_dogs'];
                break;
            default:
                return [];
                break;
        }
    }
}
