<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use enpii\enpiiCms\libs\NpActiveQuery;
use yii\db\Expression;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 * @property  $globalSearch
 */
class UserSearch extends User
{
    /**
     * @var $globalSearch
     */
    public $globalSearch;
    public $sortDefault;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_role_ids', 'profile_id', 'parent_id', 'level', 'creator_id', 'is_deleted', 'ordering_weight', 'status'], 'integer'],
            [['username', 'email', 'phone', 'mobile_phone', 'first_name', 'middle_name', 'last_name', 'password_hash', 'password_salt', 'password_reset_code', 'auth_code', 'created_at', 'updated_at', 'published_at', 'params'], 'safe'],
            [['globalSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $deleted = 0)
    {
        $query = User::find()
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user_user_role_relation}}.user_id = {{%user}}.id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id');

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->filterWhere([
            '{{%user}}.id' => $this->id,
            '{{%user}}.created_at' => $this->created_at,
            '{{%user}}.updated_at' => $this->updated_at,
            '{{%user}}.published_at' => $this->published_at,
            '{{%user}}.creator_id' => $this->creator_id,
            '{{%user}}.is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', '{{%user}}.username', $this->username])
            ->andFilterWhere(['like', '{{%user}}.email', $this->email])
            ->andFilterWhere(['like', '{{%user}}.phone', $this->phone])
            ->andFilterWhere(['like', '{{%user}}.first_name', $this->first_name])
            ->andFilterWhere(['like', '{{%user_role}}.id', $this->user_role_ids])
            ->andFilterWhere(['like', '{{%user}}.middle_name', $this->middle_name])
            ->andFilterWhere(['like', '{{%user}}.last_name', $this->last_name]);
        if ($this->globalSearch) {
            $query->filterWhere(['like', '{{%user}}.id', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.email', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.phone', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.first_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.middle_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.last_name', $this->globalSearch]);
        }

        // Return things not in trash it $deleted not set
        if (!$deleted) {
            $query->andFilterWhere(['=', '{{%user}}.is_deleted', $this->is_deleted]);
        }
        if($this->sortDefault){
            $query->orderBy([$this->sortDefault => SORT_DESC]);
        }
        return $dataProvider;
    }

    public function searchByRole($params, $role = 4)
    {

        $query = User::find()
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user_user_role_relation}}.user_id = {{%user}}.id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id');
        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->status) {
            $query->filterWhere(
                ['{{%user}}.status' => $this->status]
            );
        }

        $query->filterWhere([
            '{{%user}}.id' => $this->id,
            '{{%user}}.created_at' => $this->created_at,
            '{{%user}}.updated_at' => $this->updated_at,
            '{{%user}}.published_at' => $this->published_at,
            '{{%user}}.creator_id' => $this->creator_id,
            '{{%user}}.is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', '{{%user}}.username', $this->username])
            ->andFilterWhere(['like', '{{%user}}.email', $this->email])
            ->andFilterWhere(['like', '{{%user}}.phone', $this->phone])
            ->andFilterWhere(['like', '{{%user}}.first_name', $this->first_name])
            ->andFilterWhere(['like', '{{%user_role}}.id', $this->user_role_ids])
            ->andFilterWhere(['like', '{{%user}}.middle_name', $this->middle_name])
            ->andFilterWhere(['like', '{{%user}}.last_name', $this->last_name])
            ->andFilterWhere(['=', '{{%user_role}}.id', $role]);
        if ($this->globalSearch) {
            $query->filterWhere(['like', '{{%user}}.id', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.email', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.phone', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.first_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.middle_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.last_name', $this->globalSearch]);
        }

        // Return things not in trash it $deleted not set
        $query->andFilterWhere(['=', '{{%user}}.is_deleted', 0]);
        if($this->sortDefault){
            $query->orderBy([$this->sortDefault => SORT_DESC]);
        }
        return $dataProvider;
    }

    public function searchPendingTechnician($params)
    {

        $query = User::find()
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user_user_role_relation}}.user_id = {{%user}}.id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id');
        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->filterWhere(
            ['{{%user}}.status' => User::_STATUS_PENDING]
        );

        $query->filterWhere([
            '{{%user}}.id' => $this->id,
            '{{%user}}.created_at' => $this->created_at,
            '{{%user}}.updated_at' => $this->updated_at,
            '{{%user}}.published_at' => $this->published_at,
            '{{%user}}.creator_id' => $this->creator_id,
            '{{%user}}.is_deleted' => $this->is_deleted,
        ]);

        $query->filterWhere(['like', '{{%user}}.username', $this->username])
            ->andFilterWhere(['like', '{{%user}}.email', $this->email])
            ->andFilterWhere(['like', '{{%user}}.phone', $this->phone])
            ->andFilterWhere(['like', '{{%user}}.first_name', $this->first_name])
            ->andFilterWhere(['like', '{{%user_role}}.id', $this->user_role_ids])
            ->andFilterWhere(['like', '{{%user}}.middle_name', $this->middle_name])
            ->andFilterWhere(['like', '{{%user}}.last_name', $this->last_name])
            ->andFilterWhere(['=', '{{%user_role}}.id', User::_TECH_EXTERNAL_ID]);
        if ($this->globalSearch) {
            $query->filterWhere(['like', '{{%user}}.id', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.email', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.phone', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.first_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.middle_name', $this->globalSearch]);
            $query->orFilterWhere(['like', '{{%user}}.last_name', $this->globalSearch]);
        }

        // Return things not in trash it $deleted not set
        $query->andFilterWhere(['=', '{{%user}}.is_deleted', 0]);
        if($this->sortDefault){
            $query->orderBy([$this->sortDefault => SORT_DESC]);
        }
        return $dataProvider;
    }

    public function searchStandard($params, $deleted = 0)
    {

        $query = User::find()
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user_user_role_relation}}.user_id = {{%user}}.id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id');
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere(['{{%user_role}}.id' => User::_STANDARD_ID]);


        // Return things not in trash it $deleted not set
        if ($this->globalSearch) {
            $query->andFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
        }
        if (!$deleted) {
            $query = $query->andFilterWhere(['{{%user}}.is_deleted' => 0]);;
        }
        return $dataProvider;
    }

    public function searchTechnician($params, $deleted = 0)
    {
        $query = User::find()
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user_user_role_relation}}.user_id = {{%user}}.id')
            ->join('LEFT JOIN', '{{%user_role}}', '{{%user_user_role_relation}}.user_role_id = {{%user_role}}.id');
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere(['{{%user_role}}.id' => User::_TECH_EXTERNAL_ID]);

        // Return things not in trash it $deleted not set
        if ($this->globalSearch) {
            $query->andFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
        }
        if (!$deleted) {
            $query = $query->andFilterWhere(['{{%user}}.is_deleted' => 0]);;
        }
        return $dataProvider;
    }

    public function searchTechnicianNotAssign($params, $deleted = 0)
    {
        $query = User::find()
            ->join('LEFT OUTER JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.technician_id')
            ->join('LEFT JOIN', '{{%user_user_role_relation}}', '{{%user}}.id = {{%user_user_role_relation}}.user_id');

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        $query->where('{{%osr_item}}.technician_id IS NULL')
            ->andWhere('{{%user_user_role_relation}}.user_role_id=' . self::_TECH_EXTERNAL_ID)
            ->limit(20)->all();

        // Return things not in trash it $deleted not set
        if ($this->globalSearch) {
            $query->andFilterWhere(['like', '{{%user}}.username', $this->globalSearch]);
        }
        if (!$deleted) {
            $query = $query->andFilterWhere(['{{%user}}.is_deleted' => 0]);;
        }
        return $dataProvider;
    }


    public function searchCustomer($params, $deleted = 0)
    {
        $query = User::find()->join('LEFT JOIN', '{{%osr_item}}', '{{%user}}.id = {{%osr_item}}.creator_id');

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->filterWhere([
            'and',
            ['=', '{{%osr_item}}.technician_id', Yii::$app->user->id],
            ['=', '{{%osr_item}}.status', OsrItem::_STATUS_SUCCESSFUL],
        ]);

        $query->andFilterWhere([
            '{{%user}}.id' => $this->id,
            '{{%user}}.email' => $this->email,
            '{{%user}}.first_name' => $this->first_name,
            '{{%user}}.last_name' => $this->last_name,
            '{{%user}}.is_deleted' => 0,
        ]);

        if ($this->globalSearch) {
            $query->andFilterWhere(
                [
                    'or',
                    ['like', '{{%user}}.id', $this->globalSearch],
                    ['like', 'email', $this->globalSearch],
                    ['like', 'mobile_phone', $this->globalSearch],
                    ['like', 'first_name', $this->globalSearch],
                    ['like', 'last_name', $this->globalSearch]
                ]
            );
        }

        return $dataProvider;

    }
}
