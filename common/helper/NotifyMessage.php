<?php
namespace common\helper;

/**
 * Created by PhpStorm.
 * User: quocanhnguyen
 * Date: 9/26/16
 * Time: 11:09 AM
 */

use common\models\Option;
use yii;
use yii\helpers\Html;

class NotifyMessage
{

    /*
     * Get the content for email send to technician when technician register
     * return content string.
     */
    public static function getTechnicianRegisterToTechnician($params = null)
    {
        $strContent = 'You have used this email to register to be a Technician with us. We will inform you when your account is activated';

        return $strContent;
    }

    /*
     * Get the content for email send to admin when user register
     * return content string.
     */
    public static function getTechnicianRegisterToAdmin($params = null)
    {
        $strContent = 'A External Technician is registered, review new account <a href="' . Yii::$app->urlManager->createAbsoluteUrl('admin/user') . '">here </a>';

        return $strContent;
    }

    /*
     * Get the content for email send to admin when user register
     * return content string.
     */
    public static function getUserRegisterToUser($params = null)
    {
        if (Option::get('user_validation_enable') == 1) {
            $strContent = 'Hello ' . $params['userName'] . ',<br/>
Welcome To ' . Yii::$app->name . '<br/>

Thank you for joining!, we have sent you a separate email that contains your activation link:<br/>

' . $params['activeLink'];
        } else {
            $strContent = 'Hello ' . $params['userName'] . ',<br/>
Welcome To ' . Yii::$app->name . '<br/>

Thank you for joining!';
        }

        return $strContent;
    }

    /*
    * Get the content for email send to technician when technician apply to OSR item
    * return content string.
    */
    public static function getOsrApplyToTechnician($params = null)
    {
        $strContent = 'You have applied for ' . $params['osr_name'] . ' online service request
 ' . Html::a('View detail', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . '<br/>
Please waiting for user accept. Thank you!<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to user when technician apply to OSR item
    * return content string.
    */
    public static function getOsrApplyToUser($params = null)
    {
        $strContent = 'Technician: ' . $params['technician_name'] . ' (' . $params['technician_email'] . ') (' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $params['technician_id']])) . ') applied for your OSR ' . $params['osr_name'] . ' (' . Html::a('View Detail', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . ')<br/>
Please Preview (' . Html::a('View Applicant', Yii::$app->urlManager->createAbsoluteUrl(['osr-applicant/view', 'id' => $params['applicant_id']])) . ')<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to admin when technician apply to OSR item
    * return content string.
    */
    public static function getOsrApplyToAdmin($params = null)
    {
        $strContent = 'Technician: ' . $params['technician_name'] . ' (' . $params['technician_email'] . ') (' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $params['technician_id']])) . ') applied for your Ors ' . $params['osr_name'] . ' (' . Html::a('View Detail', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . ')<br/>
Please Preview (' . Html::a('View Applicant', Yii::$app->urlManager->createAbsoluteUrl(['osr-applicant/view', 'id' => $params['applicant_id']])) . ')<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to user when user accepted for applicant
    * return content string.
    */
    public static function getApplicantAcceptedToUser($params = null)
    {
        $strContent = 'User: ' . $params['user_name'] . '
(' . $params['user_email'] . ') (' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $params['user_id']])) . ')
accept Technician: ' . $params['technician_name'] . ' (' . $params['technician_email'] . ') to work on OSR ' . $params['osr_name'] . '
 (' . Html::a('View Details', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . ')<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to technician when user accepted for applicant
    * return content string.
    */
    public static function getApplicantAcceptedToTechnician($params = null)
    {
        $strContent = 'User: ' . $params['user_name'] . '
(' . $params['user_email'] . ')(' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $params['user_id']])) . ')
accept Technician: ' . $params['technician_name'] . ' (' . $params['technician_email'] . ') to work on OSR ' . $params['osr_name'] . '
(' . Html::a('View Details', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . ')<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to admin when user accepted for applicant
    * return content string.
    */
    public static function getApplicantAcceptedToAdmin($params = null)
    {
        $strContent = 'User: ' . $params['user_name'] . '
(' . $params['user_email'] . ')(' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $params['user_id']])) . ')
accept Technician: ' . $params['technician_name'] . ' (' . $params['technician_email'] . ') to work on OSR ' . $params['osr_name'] . '
(' . Html::a('View Details', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $params['osr_id']])) . ')<br/>';

        return $strContent;
    }

    /*
    * Get the content for email send to Technician when admin activate for technician account
    * return content string.
    */
    public static function getActiveTechnicianAccount($params = null)
    {
        $strContent = 'Your account has been activated. Please update your profile <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['technician/profile']) . '">here</a>';

        return $strContent;
    }

    /*
    * Get the content for email send to Technician when admin deny for technician account
    * return content string.
    */
    public static function getDenyTechnicianAccount($params = null)
    {
        $strContent = 'Your account has been denied. Please <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['page/contact']) . '">contact us</a> for more details';

        return $strContent;
    }


    /*
    * Get the content for email send to User when completed payment
    * return content string.
    */
    public static function getDepositCompletedToUser($params = null)
    {
        $strContent = 'You deposited successfully for OSR item ID #' . $params['osr_id'] . ', Order ID is #' . $params['order_id'];

        return $strContent;
    }

    /*
    * Get the content for email send to Admin when completed payment
    * return content string.
    */
    public static function getDepositCompletedToAdmin($params = null)
    {
        $strContent = 'The user ' . $params['user_email'] . ' has completed payment for OSR item ID #' . $params['osr_id'] . ', Order ID is #' . $params['order_id'];

        return $strContent;
    }

    /*
        * Get the content for email send to Technician when admin activate for technician account
        * return content string.
        */
    public static function getCreateOsrNotificationToUser($params = null)
    {
        $strContent = 'You created a OSR item successfully, please check again <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['osr-item/view', 'id' => $params['id']]) . '">here</a>';

        return $strContent;
    }

    /*
    * Get the content for email send to User when Admin want to payment reminder
    * return content string.
    */
    public static function getPaymentReminder($params = null)
    {
        $strContent = 'You created a OSR item, please <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['osr-item/deposited']) . '">click here</a> to pay.';

        return $strContent;
    }

    /*
    * Get the content for email send to User when Admin want to payment reminder
    * return content string.
    */
    public static function getContentForSendInvoice($params = null)
    {
        $strContent = 'The Technician ' . $params['technician_name'] . ' of OSR item ' . $params['osr_name'] . ' has sent for you a invoice, Please check again <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['technician-invoice/accept-invoice', 'osr_item_id' => $params['osr_item_id']]) . '" >here</a>';

        return $strContent;
    }

    /*
    * Get the content for email send to Admin when technician send invoice
    * return content string.
    */
    public static function getContentForSendInvoiceToAdmin($params = null)
    {
        $strContent = 'The Technician ' . $params['technician_name'] . ' of OSR item ' . $params['osr_name'] . ' has sent for you a invoice, Please check again <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['technician-invoice/accept-invoice', 'osr_item_id' => $params['osr_item_id']]) . '" >here</a>';

        return $strContent;
    }

    /*
    * Get the content for email send to User when Admin want to payment reminder
    * return content string.
    */
    public static function getContentAcceptedInvoice($params = null)
    {
        $strContent = 'The user ' . $params['user_name'] . ' accepted invoice of OSR ' . $params['osr_name'] . ', Please check again <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['osr-item/on-going-osr']) . '" >here</a>';

        return $strContent;
    }

    public static function getContentForMarkToDone($params = null)
    {
        $strContent = 'Technician mark to done OSR Item ' . $params['osr_item_name'] . ', please <a href="' . Yii::$app->urlManagerFrontend->createAbsoluteUrl(['osr-item/view-invoice', 'id' => $params['osr_item_id']]) . '">click here</a> to pay.';

        return $strContent;
    }



/*
* Get the content for email send to Admin when completed payment
* return content string.
*/
public
static function getPaidInvoiceToAdmin($params = null)
{
    $strContent = 'The user ' . $params['user_name'] . ' has completed payment for OSR item ID #' . $params['osr_item_id'] . ', Invoice ID is #' . $params['invoice_id'];

    return $strContent;
}

/*
* Get the content for email send to Consumer when completed payment
* return content string.
*/
public
static function getPaidInvoiceToConsumer($params = null)
{
    $strContent = 'You have completed payment for OSR item ID #' . $params['osr_item_id'] . ', Invoice ID is #' . $params['invoice_id'];

    return $strContent;
}

/*
* Get the content for email send to Consumer when completed payment
* return content string.
*/
public
static function getPaidInvoiceToTechnician($params = null)
{
    $strContent = 'The user ' . $params['user_name'] . ' has completed payment for OSR item ID #' . $params['osr_item_id'] . ', Invoice ID is #' . $params['invoice_id'];

    return $strContent;
}

}