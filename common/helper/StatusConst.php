<?php
/**
 * Created by PhpStorm.
 * User: quocanhnguyen
 * Date: 9/27/16
 * Time: 3:14 PM
 */

namespace common\helper;
use Yii;

class StatusConst
{
    //    OSR item status
    //
    const _STATUS_DRAFT = 10;
    //after confirm the osr item
    const _STATUS_CONFIRMED = 11;

    //status after create
    const _STATUS_PENDING = 12;
    //after make deposit
    const _STATUS_DEPOSITED = 13;
    //open to technician apply
    const _STATUS_OPEN_TO_EXTERNAL = 14;
    //status after user accept applicant
    const _STATUS_ASSIGNED = 15;
    //status of applicant after was accepted
    const _STATUS_ACCEPTED = 16;
    //status fail osr item job
    const _STATUS_FAILED = 17;
    //status after completed osr item
    const _STATUS_SUCCESSFUL = 18;
    //status of Applicant, after technician done osr item
    const _STATUS_DONE = 19;
    //status of OSR item, after user accepted invoice of Technician
    const _STATUS_ONGOING = 21;
    // status of OSR item, after user mark cancel
    const _STATUS_CANCEL = 27;

    const _STATUS_REFUNDED = 29;
    //status of OSR item, after Technician mark as done
    const _STATUS_WAIT_FOR_PAYMENT = 24;

    //    Payment status
    //after create order
    const _ORDER_PENDING = 0;
    //after completed payment
    const _ORDER_COMPLETED = 1;

    public static function getOsrItemStatuses()
    {
        return [
            static::_STATUS_DRAFT => Yii::t(_NP_TEXT_CATE, 'New'),
            static::_STATUS_PENDING => Yii::t(_NP_TEXT_CATE, 'Pending'),
            static::_STATUS_DEPOSITED => Yii::t(_NP_TEXT_CATE, 'Deposited'),
            static::_STATUS_OPEN_TO_EXTERNAL => Yii::t(_NP_TEXT_CATE, 'Open to external'),
            static::_STATUS_ASSIGNED => Yii::t(_NP_TEXT_CATE, 'Assigned'),
            static::_STATUS_ACCEPTED => Yii::t(_NP_TEXT_CATE, 'Accepted'),
            static::_STATUS_ONGOING => Yii::t(_NP_TEXT_CATE, 'On-going'),
            static::_STATUS_WAIT_FOR_PAYMENT => Yii::t(_NP_TEXT_CATE, 'Waiting for Payment'),
            static::_STATUS_FAILED => Yii::t(_NP_TEXT_CATE, 'Failed'),
            static::_STATUS_CANCEL => Yii::t(_NP_TEXT_CATE, 'Cancelled'),
            static::_STATUS_SUCCESSFUL => Yii::t(_NP_TEXT_CATE, 'Successful'),
        ];
    }
}