<?php
use common\models\SystemNotification;

/* @var $this yii\web\View */
/* @var $content string */

?>

<?= $content ?>
<br>
<br>
<i>(<?= Yii::t('app', 'This is automatic email - Please do not reply') ?>)</i>
