<?php
use common\models\SystemNotification;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$activateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-user', 'email' => $user->email, 'token' => $user->auth_code]);

$strContent = 'Hello ' . $user->username . ',<br/>
Welcome To ' . Yii::$app->name . '<br/>

Thank you for joining!, we have sent you a separate email that contains your activation link:<br/>

' . $activateLink ;

?>
<?= $strContent?>
