<?php
use yii\helpers\Html;
use common\models\SystemNotification;

/* @var $this yii\web\View */
/* @var $to string */

$strContent = 'A External Technician is registered, review new account <a href="' . Yii::$app->urlManager->createAbsoluteUrl('admin/user') . '">here </a>';

$systemNotify = new SystemNotification();
$systemNotify->name = 'Technician External Register';
$systemNotify->content = $strContent;
$systemNotify->receiver_emails = $to;
$systemNotify->is_emailed = 1;
$systemNotify->is_smsed = 0;

$systemNotify->save();
?>
<p>
    <?= $strContent?>
</p>
<i>(This is automatic email - Please no reply)</i>