<?php
use yii\helpers\Html;
use common\models\SystemNotification;

/* @var $this yii\web\View */
/* @var $to string */

$strContent = 'You have used this email to register to be a Technician with us. We will inform you when your account is activated';

$systemNotify = new SystemNotification();
$systemNotify->name = 'Technician External Register';
$systemNotify->content = $strContent;
$systemNotify->receiver_emails = $to;
$systemNotify->is_emailed = 1;
$systemNotify->is_smsed = 0;

$systemNotify->save();
?>
<p>
    <?= $strContent ?>
</p>
<i>(This is automatic email - Please no reply)</i>