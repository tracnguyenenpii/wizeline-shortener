<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $technician common\models\User */
/* @var $osrItem common\models\OsrItem */
/* @var $osrApplicant common\models\OsrItemApplicant */
?>
Technician: <?= $technician->getFullName()  ?> (<?= $technician->email ?>) (<?= Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view','id' => $technician->id]))?>) applied for your Ors <?= $osrItem->name ?> (<?= Html::a('View Detail', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view','id' => $osrItem->id]))?>)<br/>
Please Preview (<?= Html::a('View Applicant',Yii::$app->urlManager->createAbsoluteUrl(['osr-applicant/view','id' => $osrApplicant->id]))?>)<br/>
<i>(This is automatic email - Please no reply)</i>