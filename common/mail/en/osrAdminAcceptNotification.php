<?php
use yii\helpers\Html;
use common\models\SystemNotification;

/* @var $this yii\web\View */
/* @var $technician common\models\User */
/* @var $osrItem common\models\OsrItem */
/* @var $osrApplicant common\models\OsrItemApplicant */
/* @var $to string */

$strContent = 'User: ' . $osrItem->user->getFullName() . '
(' . $osrItem->user->email . ') (' . Html::a('View Technician', Yii::$app->urlManager->createAbsoluteUrl(['user/view', 'id' => $osrItem->user->id])) . ')
accept Technician: ' . $technician->getFullName() . ' (' . $technician->email . ') to work on OSR ' . $osrItem->name . '
(' . Html::a('View Details', Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view', 'id' => $osrItem->id])) . ')<br/>';

$systemNotify = new SystemNotification();
$systemNotify->name = 'Technician External Register';
$systemNotify->content = $strContent;
$systemNotify->receiver_emails = $to;
$systemNotify->is_emailed = 1;
$systemNotify->is_smsed = 0;

$systemNotify->save();
?>
<?= $strContent?>
<i>(This is automatic email - Please no reply)</i>