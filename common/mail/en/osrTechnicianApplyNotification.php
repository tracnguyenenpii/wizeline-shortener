<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $technician common\models\User */
/* @var $osrItem common\models\OsrItem */
/* @var $osrApplicant common\models\OsrItemApplicant */
?>
You have applied for <?= $osrItem->name ?> online service request
 <?= Html::a('View detail',Yii::$app->urlManager->createAbsoluteUrl(['osr-item/view','id' => $osrItem->id]))?><br/>
Please waiting for user accept. Thank you!<br/>
<i>(This is automatic email - Please no reply)</i>