<?php

/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/15/16 5:04 PM
 */

namespace api\controllers;

use common\models\OsrItem;
use common\models\User;
use yii;
use common\libs\ClsControllerApi;
use yii\web\Response;

class OsrItemController extends ClsControllerApi
{

    public function actionList($api_key, $api_secret)
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($user = User::findOne(['api_key' => $api_key, 'api_secret' => $api_secret])) {

            $arrItems = [];
            if ($osrItems = OsrItem::getListOsrByApi($user->id)) {
                foreach ($osrItems as $key => $osrItem) {

                    $arrItems[$key]['code'] = $osrItem->code;
                    $arrItems[$key]['name'] = $osrItem->name;
                    $arrItems[$key]['description'] = $osrItem->description;
                    $arrItems[$key]['created'] = $osrItem->getCreatedAt();
                    $arrItems[$key]['referred_date'] = $osrItem->referred_date;
                    $arrItems[$key]['referred_time'] = $osrItem->referred_time;
                }

            }

            return [

                'errorCode' => 200,
                'errorMessage' => Yii::t(_NP_TEXT_CATE, ''),
                'result' => [
                    'items' => $arrItems
                ],
            ];
        }

        return [
            'errorCode' => 403, // don't permission
            'errorMessage' => Yii::t(_NP_TEXT_CATE, 'API Key or API Secret not correct.'), // not found
            'result' => [

            ],
        ];

    }

    public function actionView($api_key, $api_secret, $osr_code)
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($user = User::findOne(['api_key' => $api_key, 'api_secret' => $api_secret])) {

            $arrItem = [];
            if ($osrItem = OsrItem::findOne(['code' => $osr_code, 'is_deleted' => 0])) {
                $arrItem['code'] = $osrItem->code;
                $arrItem['name'] = $osrItem->name;
                $arrItem['description'] = $osrItem->description;
                $arrItem['created_at'] = $osrItem->getCreatedAt();
                $arrItem['referred_date'] = $osrItem->referred_date;
                $arrItem['referred_time'] = $osrItem->referred_time;

            }

            return [

                'errorCode' => 200,
                'errorMessage' => Yii::t(_NP_TEXT_CATE, ''),
                'result' => [
                    'item' => $arrItem
                ],
            ];
        }

        return [
            'errorCode' => 403, // don't permission
            'errorMessage' => Yii::t(_NP_TEXT_CATE, 'API Key or API Secret not correct.'), // not found
            'result' => [

            ],
        ];

    }

    public function actionError()
    {
        $result = [
            'errorCode' => 404, // not found
            'errorMessage' => Yii::t(_NP_TEXT_CATE, 'Page not found.'), // not found
            'result' => [

            ],
        ];


        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return $result;
    }
}
