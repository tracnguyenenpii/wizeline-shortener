<?php

/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/15/16 5:04 PM
 */

namespace api\controllers;

use common\models\ShortenerItem;
use enpii\enpiiCms\helpers\DateTimeHelper;
use yii;
use common\libs\ClsControllerApi;

class SiteController extends ClsControllerApi
{
    public function actionIndex() {
        $result = [
            'code' => 200, // success
            'response' => [
                'unique' => 0,
                'message' => '',
            ],
        ];


        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function beforeAction($action)
    {
        if ($action->id == 'shorten') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionShorten() {
        $model = new ShortenerItem();
        if (!empty($_REQUEST['original_url'])) {
            if (empty($_REQUEST['slug'])) {
                $model->slug = uniqid();
            } else {
                $model->slug = $_REQUEST['slug'];
            }
            $model->original_url = $_REQUEST['original_url'];
            $model->created_at = DateTimeHelper::toDbFormat();
            $model->updated_at = DateTimeHelper::toDbFormat();


            if ($model->save()) {
                $result = [
                    'code' => 200, // success
                    'response' => [
                        'message' => 'Done',
                        'shortened_url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['shortener-item/redirect', 'slug' => $model->slug]), ['shortener-item/redirect', 'slug' => $model->slug]
                    ],
                ];
            } else {
                $result = [
                    'code' => 456, // failed
                    'response' => [
                        'unique' => 0,
                        'message' => 'Failed in Validation',
                    ],
                ];
            }
        } else {
            $result = [
                'code' => 123, // failed
                'response' => [
                    'unique' => 0,
                    'message' => 'Failed',
                ],
            ];
        }




        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionError() {
        $result = [
            'code' => 404, // not found
            'response' => [
                'unique' => 0,
                'message' => '',
            ],
        ];


        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return $result;
    }
}
