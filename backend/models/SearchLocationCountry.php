<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LocationCountry;
use enpii\enpiiCms\libs\NpActiveQuery;

/**
 * SearchLocationCountry represents the model behind the search form about `common\models\LocationCountry`.
 * @property  $globalSearch
 */
class SearchLocationCountry extends LocationCountry
{
    /**
     * @var $globalSearch
     */
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_default', 'ordering_weight'], 'integer'],
            [['name', 'name_en', 'slug', 'country_code_iso2', 'country_code_iso3', 'language_code_iso2', 'language_code_iso3'], 'safe'],
            [['globalSearch'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $deleted = 0)
    {
        $query = LocationCountry::find();

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_default' => $this->is_default,
            'ordering_weight' => $this->ordering_weight,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'country_code_iso2', $this->country_code_iso2])
            ->andFilterWhere(['like', 'country_code_iso3', $this->country_code_iso3])
            ->andFilterWhere(['like', 'language_code_iso2', $this->language_code_iso2])
            ->andFilterWhere(['like', 'language_code_iso3', $this->language_code_iso3]);
        if($this->globalSearch) {
            $query->orFilterWhere(['like','id', $this->globalSearch]);
            $query->orFilterWhere(['like','name', $this->globalSearch]);
            $query->orFilterWhere(['like','name_en', $this->globalSearch]);
            $query->orFilterWhere(['like','slug', $this->globalSearch]);
            $query->orFilterWhere(['like','country_code_iso2', $this->globalSearch]);
            $query->orFilterWhere(['like','country_code_iso3', $this->globalSearch]);
            $query->orFilterWhere(['like','language_code_iso2', $this->globalSearch]);
            $query->orFilterWhere(['like','language_code_iso3', $this->globalSearch]);
            $query->orFilterWhere(['like','is_default', $this->globalSearch]);
            $query->orFilterWhere(['like','ordering_weight', $this->globalSearch]);
        }

        // Return things not in trash it $deleted not set
        if (!$deleted) {
            $query = $query->notDeleted();
        }
        return $dataProvider;
    }
}