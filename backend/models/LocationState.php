<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LocationState as LocationStateModel;
use enpii\enpiiCms\libs\NpActiveQuery;

/**
 * LocationState represents the model behind the search form about `common\models\LocationState`.
 * @property  $globalSearch
 */
class LocationState extends LocationStateModel
{
    /**
     * @var $globalSearch
     */
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region', 'country_id', 'is_default', 'ordering_weight', 'is_deleted', 'status'], 'integer'],
            [['name', 'name_en', 'slug', 'code', 'full_name'], 'safe'],
            [['globalSearch'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $deleted = 0)
    {
        $query = LocationStateModel::find();

        /* @var $query NpActiveQuery */

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'region' => $this->region,
            'country_id' => $this->country_id,
            'is_default' => $this->is_default,
            'ordering_weight' => $this->ordering_weight,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'full_name', $this->full_name]);
        if($this->globalSearch) {
            $query->orFilterWhere(['like','id', $this->globalSearch]);
            $query->orFilterWhere(['like','name', $this->globalSearch]);
            $query->orFilterWhere(['like','name_en', $this->globalSearch]);
            $query->orFilterWhere(['like','slug', $this->globalSearch]);
            $query->orFilterWhere(['like','code', $this->globalSearch]);
            $query->orFilterWhere(['like','region', $this->globalSearch]);
            $query->orFilterWhere(['like','country_id', $this->globalSearch]);
            $query->orFilterWhere(['like','full_name', $this->globalSearch]);
            $query->orFilterWhere(['like','is_default', $this->globalSearch]);
            $query->orFilterWhere(['like','ordering_weight', $this->globalSearch]);
            $query->orFilterWhere(['like','is_deleted', $this->globalSearch]);
            $query->orFilterWhere(['like','status', $this->globalSearch]);
        }

        // Return things not in trash it $deleted not set
        if (!$deleted) {
            $query = $query->notDeleted();
        }
        return $dataProvider;
    }
}