/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 * http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
 */
function mergeOptions(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

var NpApp = {};

/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

var arrAlert = [];
NpApp.alert = (function () {
    'use strict';
    return {
        options: {
            selector: '.np-alert',
            initTimeout: 3000,
            nextTimeout: 1000
        },
        init: function (inputOptions) {
            this.options = mergeOptions(this.options, inputOptions);

            var i = 0;
            $(this.options.selector).each(function () {
                arrAlert[i] = $(this).attr('id');
                i++;
            });

            setTimeout(function () {
                hideAlert(0);
            }, this.options.initTimeout);
        }
    }
}());
function hideAlert(index) {
    $('#' + arrAlert[index]).fadeToggle();
    if (typeof(arrAlert[index + 1]) != 'undefined') {
        setTimeout(function () {
            NpApp.alert.next(index + 1);
        }, NpApp.alert.options.nextTimeout);
    }
}

/**
 * Created by hungtran on 9/24/16.
 */
NpApp.coupon = (function () {
    'use strict';
    return {
        init: function (selector) {
            $('.field-static-amount').toggleClass('hidden');
            $(selector).change(function () {
                var checked =$('#coupon-discount span.checked input').val();
                $('.field-static-amount').toggleClass('hidden');
                $('.field-percentage').toggleClass('hidden');
            });
        }
    }
}());

/**
 * Created by npbtrac@yahoo.com on 8/16/16.
 */

NpApp.miscs = (function () {
    'use strict';

    return {
        sidebarMenuActive: function () {
            jQuery('#sidebar-primary-menu li li.active').each(function () {
                jQuery(this).parent().parent().addClass('open').addClass('active');
            });
        },
        init: function (inputOptions) {
            NpApp.miscs.sidebarMenuActive();

            jQuery('body').on('click', ".colorbox.iframe", function () {
                jQuery(this).colorbox({iframe: true, width: "80%", height: "80%", fixed: true});
            });

            jQuery('body').on('click', ".colorbox", function () {
                jQuery(this).colorbox();
            });
        }
    }
}());

/**
 * Created by npbtrac@yahoo.com on 8/13/16.
 */
NpApp.main = (function() {
    'use strict';
    var $body;
    return {
        init: function() {
            // Cache some variables
            $body = $('body');
            NpApp.alert.init({initTimeout: 5000, selector: '.np-alert .alert'});
            NpApp.coupon.init('.coupon-discount');
        },
        getBody: function() {
            return $body;
        }
    };
}());

jQuery(document).ready(function(){
    // jQuery wrapper
    (function ($) {

        NpApp.miscs.init();
        NpApp.main.init();
    })(jQuery);
});


