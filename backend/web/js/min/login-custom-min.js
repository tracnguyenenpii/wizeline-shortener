/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 * http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
 */
function mergeOptions(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

var NpApp = {};

/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

NpApp.alert = (function () {
    'use strict';

    var arrAlert = [];
    var options = {
        selector: '.np-alert',
        initTimeout: 3000,
        nextTimeout: 1000
    };

    return {
        next: function (i) {
            $('#' + arrAlert[i]).fadeToggle();
            if (typeof(arrAlert[i + 1]) != 'undefined') {
                setTimeout(function () {
                    NpApp.alert.next(i + 1);
                }, options.nextTimeout);
            }
        },
        init: function (inputOptions) {
            options = mergeOptions(options, inputOptions);

            var i = 0;
            $(options.selector).each(function () {
                arrAlert[i] = $(this).attr('id');
                i++;
            });

            setTimeout(function () {
                NpApp.alert.next(0);
            }, options.initTimeout);
        }
    }
}());



