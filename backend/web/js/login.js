/**
 * Created by npbtrac@yahoo.com on 8/13/16.
 */

NpApp.main = (function() {
    'use strict';
    var $body;
    return {
        init: function() {
            // Cache some variables
            $body = $('body');
            NpApp.alert.init({initTimeout: 3000, selector: '.np-alert .alert'});
        },
        getBody: function() {
            return $body;
        }
    };
}());

NpApp.main.init();

jQuery(document).ready(function(){
    // jQuery wrapper
    (function ($) {

    })(jQuery);
});
