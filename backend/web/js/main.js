/**
 * Created by npbtrac@yahoo.com on 8/13/16.
 */
NpApp.main = (function() {
    'use strict';
    var $body;
    return {
        init: function() {
            // Cache some variables
            $body = $('body');
            NpApp.alert.init({initTimeout: 5000, selector: '.np-alert .alert'});
            NpApp.coupon.init('.coupon-discount');
        },
        getBody: function() {
            return $body;
        }
    };
}());

jQuery(document).ready(function(){
    // jQuery wrapper
    (function ($) {

        NpApp.miscs.init();
        NpApp.main.init();
    })(jQuery);
});
