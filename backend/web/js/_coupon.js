/**
 * Created by hungtran on 9/24/16.
 */
NpApp.coupon = (function () {
    'use strict';
    return {
        init: function (selector) {
            $('.field-static-amount').toggleClass('hidden');
            $(selector).change(function () {
                var checked =$('#coupon-discount span.checked input').val();
                $('.field-static-amount').toggleClass('hidden');
                $('.field-percentage').toggleClass('hidden');
            });
        }
    }
}());