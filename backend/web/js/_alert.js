/**
 * Created by npbtrac@yahoo.com on 8/7/16.
 */

var arrAlert = [];
NpApp.alert = (function () {
    'use strict';
    return {
        options: {
            selector: '.np-alert',
            initTimeout: 3000,
            nextTimeout: 1000
        },
        init: function (inputOptions) {
            this.options = mergeOptions(this.options, inputOptions);

            var i = 0;
            $(this.options.selector).each(function () {
                arrAlert[i] = $(this).attr('id');
                i++;
            });

            setTimeout(function () {
                hideAlert(0);
            }, this.options.initTimeout);
        }
    }
}());
function hideAlert(index) {
    $('#' + arrAlert[index]).fadeToggle();
    if (typeof(arrAlert[index + 1]) != 'undefined') {
        setTimeout(function () {
            NpApp.alert.next(index + 1);
        }, NpApp.alert.options.nextTimeout);
    }
}