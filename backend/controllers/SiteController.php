<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 7/1/16 9:57 PM
 */

namespace backend\controllers;

use common\libs\ClsController;
use enpii\enpiiCms\libs\NpHttpException;
use enpii\enpiiCms\models\form\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii;
use common\models\CouponPin;

/**
 * SiteController
 * For handling some basic actions
 * index, error, login, logout
 */
class SiteController extends ClsController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                    ],
                    'matchCallback' => function () {
                        if (Yii::$app->user->can('backend-login')) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get']
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        if (!Yii::$app->user->isGuest) {
            $this->layout = '@enpii/enpiiCms/views/layouts/backend/main';
            Yii::$app->view->params['_sidebar_path'] = "@app/views/layouts/_sidebar";

            $totalETTicket = CouponPin::find()->where(['like','pin','LN1'])->count();
            $totalETTicketClaimed = CouponPin::find()->where(['like','pin','LN1'])->andFilterWhere(['is_obtained' => 1])->count();
            $totalETTicketLeft = $totalETTicket - $totalETTicketClaimed;



            $totalTATicket = CouponPin::find()->where(['like','pin','LN2'])->count();
            $totalTATicketClaimed = CouponPin::find()->where(['like','pin','LN2'])->andFilterWhere(['is_obtained' => 1])->count();
            $totalTATicketLeft = $totalTATicket - $totalTATicketClaimed;
            return $this->render('dashboard',[
                'totalETTicket' => $totalETTicket,
                'totalETTicketClaimed' => $totalETTicketClaimed,
                'totalETTicketLeft' => $totalETTicketLeft,
                'totalTATicket' => $totalTATicket,
                'totalTATicketClaimed' => $totalTATicketClaimed,
                'totalTATicketLeft' => $totalTATicketLeft,

            ]);
        } else {
            return $this->redirect(Yii::$app->urlManager->createUrl('site/login'));
        }
    }

    public function actionLogin()
    {
        $msLoginForm = new LoginForm();
        // Specify this a backend login
        $msLoginForm->loginType = LoginForm::_ACCESS_BACKEND;
        if ($msLoginForm->load(Yii::$app->request->post()) && $msLoginForm->login($msLoginForm->loginType)) {
            $defaultUrl = Yii::$app->urlManager->createUrl('');
            return $this->redirect($defaultUrl);
        } else {

            // Show guides if no messages in queue
            if (!Yii::$app->session->getAllFlashes()) {
                Yii::$app->session->addFlash('info', Yii::t(_NP_TEXT_CATE, 'Enter the correct info to login.'));
            }

            $this->layout = '@enpii/enpiiCms/views/layouts/backend/login';

            // Reset password to blank when returning from submit
            $msLoginForm->password = '';
            return $this->render('@enpii/enpiiCms/views/backend/site/login', [
                'msLoginForm' => $msLoginForm,
            ]);
        }
    }

    public function actionLogout()
    {
        $url = Yii::$app->getUrlManager()->createUrl('/enpiiCms/backend/site/logout');
        return $this->redirect($url);
    }
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new NpHttpException(404, Yii::t(_NP_TEXT_CATE, 'Page not found.'));
        }

        if ($exception instanceof yii\web\HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof yii\base\Exception) {
            $name = Yii::t(_NP_TEXT_CATE, $exception->getName());
        } else {
            $name = Yii::t(_NP_TEXT_CATE, 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof yii\base\UserException) {
            $message = $exception->getMessage();
        } else {
            $message = $this->defaultMessage ?: Yii::t(_NP_TEXT_CATE, 'An internal server error occurred.');
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            if (Yii::$app->user->isGuest) {
                $this->layout = 'simple';
            } else {
                $this->layout = 'main';
            }

            return $this->render('error', [
                'name' => $name,
                'message' => $message,
                'exception' => $exception,
            ]);
        }
    }
}
