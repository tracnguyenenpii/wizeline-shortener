<?php

namespace backend\controllers;

use common\helper\NotifyMessage;
use common\helper\StatusConst;
use common\libs\ClsControllerBackend;
use common\models\ActivityLog;
use common\models\OsrItem;
use common\models\UserAttachment;
use enpii\enpiiCms\libs\NpActiveForm;
use common\models\User;
use common\models\UserSearch;
use common\libs\ClsController;
use frontend\models\ChangePasswordForm;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use enpii\enpiiCms\helpers\ArrayHelper;
use enpii\enpiiCms\helpers\DateTimeHelper;
use enpii\enpiiCms\helpers\CommonHelper;
use yii\filters\AccessControl;
use yii;
use common\models\SystemNotification;
use common\models\OsrUserReviewSearch;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends ClsControllerBackend
{
    public function behaviors()
    {
        $parentOneBehaviors = parent::behaviors();
        $thisOneBehaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
        return ArrayHelper::merge($parentOneBehaviors, $thisOneBehaviors);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionDashboard()
    {
        return $this->render('dashboard', [

        ]);
    }

    public function actionChunkUpload()
    {
        $scriptUrl = Yii::$app->request->url;
        $modelClassName = Yii::$app->request->getQueryParam('model_class');
        $className = \enpii\enpiiCms\helpers\StringHelper::basename($modelClassName);

        $id = Yii::$app->request->getQueryParam('id');
        $model = $modelClassName::findByID($id);
        if (empty($model)) {
            $model = new $modelClassName();
        }
        $options = [
            'script_url' => $scriptUrl,
            'upload_dir' => $model->getUploadTmpPath() . '/',
            'upload_url' => $model->getUploadTmpUrl() . '/',
            'param_name' => $className,
            'print_response' => false,
            'image_versions' => [
            ],
        ];

        $upload_handler = new BlueImpUploadHandler($options);
        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            return Json::encode([
                'files' => $upload_handler->response,
            ]);
        } else {
            if (!empty($_FILES[$className])) {
                return Json::encode([
                    'files' => $upload_handler->response[$className],
                ]);
            } else {
                $files = $model->getAvatarAttachments();
                return Json::encode([
                    'files' => $files,
                ]);
            }
        }
    }

    public function actionUploadAvatar()
    {
        $imageFile = UploadedFile::getInstanceByName('User[avatar_id]');
        $directory = Yii::getAlias('@root/uploads') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $path = '@webroot/uploads/' . Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;
                return Json::encode([
                    'files' => [
                        [
                            'name' => $fileName,
                            'size' => $imageFile->size,
                            "url" => $path,
                            "thumbnailUrl" => $path,
                            "deleteUrl" => 'delete-avatar?name=' . $fileName,
                            "deleteType" => "POST"
                        ]
                    ]
                ]);
            }
        }
        return '';
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        if (empty(Yii::$app->getRequest()->getQueryParam('sort'))) {
            $searchModel->sortDefault = 'created_at';
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'bulkAction' => false,
            'role' => 'user'
        ]);
    }


    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user = $this->findModel($id);

        return $this->render('view', [
            'model' => $user,
        ]);
    }

    /**
     * Create a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new User();
        $model->scenario = User::_SCENARIO_CREATE;
        $model->prepareUserRoles();

        if ($model->load(Yii::$app->request->post())) {
            $model->creator_id = Yii::$app->user->getId();
            $model->is_deleted = 0;
            $model->created_at = DateTimeHelper::toDbFormat();
            $model->updated_at = DateTimeHelper::toDbFormat();

            $model->status = User::_STATUS_PUBLISHED;
            if ($model->status == User::_STATUS_PUBLISHED) {
                $model->published_at = DateTimeHelper::toDbFormat();
            }

            $model->username = strtolower($model->username);
            if(empty($model->username)) {
                $model->username = strtolower($model->email);
            }
            $model->email = strtolower($model->email);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return NpActiveForm::validate($model);
            }

            $model->password_salt = uniqid();
            $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->password);

            if ($model->validate([
                        'first_name',
                        'last_name',
                        'mobile_phone',
                        'work_permit_number',
                        'state_ids',
                        'technician_category_ids',
                        'technician_service_type_ids',
                        'email',
                        'password',
                        'password_confirm'
                    ]
                ) && $model->save(false)
            ) {
                Yii::$app->session->addFlash('success', Yii::t(_NP_TEXT_CATE, 'Data created successfully.'));

                // Update UserRoles
                $model->saveUserRoles();
                $this->processAvatarUpload($model);
                /*
                 * insert log after register user
                 */
                // Redirect to View
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('error',
                    Yii::t(_NP_TEXT_CATE, 'Data created unsuccessfully. Please recheck the input.'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $model->prepareUserRoles();

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = DateTimeHelper::toDbFormat();

            $model->status = User::_STATUS_PUBLISHED;
            if ($model->status == User::_STATUS_PUBLISHED) {
                $model->published_at = DateTimeHelper::toDbFormat();
            }

            $model->username = strtolower($model->username);
            $model->email = strtolower($model->email);


            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return NpActiveForm::validate($model);
            }

            $model->password_hash = $model->password ? Yii::$app->getSecurity()->generatePasswordHash($model->password) : $model->password_hash;

            if ($model->validate([
                    'first_name',
                    'last_name',
                    'mobile_phone',
                    'work_permit_number',
                    'state_ids',
                    'technician_category_ids',
                    'technician_service_type_ids',
                    'email',
                    'password',
                    'password_confirm'
                ]) && $model->save(false)
            ) {
                $model->saveUserRoles();
                $this->processAvatarUpload($model);
                ActivityLog::insertLogAdminUpdateUser(Yii::$app->user->id, $model->id);
                Yii::$app->session->addFlash('success', Yii::t(_NP_TEXT_CATE, 'Data updated successfully.'));
            } else {
                Yii::$app->session->addFlash('error',
                    Yii::t(_NP_TEXT_CATE, 'Data updated unsuccessfully. Please recheck the input.'));
            }
        }

        $model->password = $model->password_confirm = '';

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $model User
     */
    protected function processAvatarUpload(&$model)
    {
        // Process Avatar
        if (!empty($model->avatar)) {

            // Insert Attachment and update avatar
            foreach ($model->avatar as $key => $filename) {
                if (!empty($filename)) {
                    $strBasename = StringHelper::basename($filename);

                    // $filename not an url or a path means new uploaded one
                    if ($strBasename == $filename) {
                        $modelUserAttachment = new UserAttachment();
                        if ($modelUserAttachment->insertUploadedFile($model->getUploadTmpPath() . '/' . $filename)) {
                            $model->avatar_id = $modelUserAttachment->id;
                            $model->save(false, ['avatar_id']);
                        }
                    }
                }
            }

            // Reset avatar for the form to render correctly
            $model->avatar = null;
        } else {
            // No uploaded files, meaning no avatar
            $model->avatar_id = null;
            $model->save(false, ['avatar_id']);
        }

        return $model;
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);

        if (!$user->userCan('super_admin')) {
            $user->delete();
            Yii::$app->session->addFlash('success', Yii::t(_NP_TEXT_CATE, 'Data deleted successfully.'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t(_NP_TEXT_CATE, 'Data deleted unsuccessfully.'));
        }

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        return $this->redirect($indexUrlQueryParams);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t(_NP_TEXT_CATE, 'The requested item does not exist.'));
        }
    }

    /**
     * Bulk action on selected items
     * @return \yii\web\Response
     */
    public function actionBulk()
    {
        $action = Yii::$app->request->post('bulk-option');
        $selection = explode(',', Yii::$app->request->post('selection')[0]);

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        if (count($selection) > 0) {
            $arrIDSuccess = [];
            $arrIDFail = [];

            foreach ($selection as $id) {
                $result = false;
                if ($action == 'delete') {
                    $strAction = Yii::t(_NP_TEXT_CATE, 'Execute Bulk Deletion.');
                    $result = $this->findModel($id)->putToTrash();
                }
                if ($result) {
                    $arrIDSuccess[] = $id;
                } else {
                    $arrIDFail[] = $id;
                }
            }

            $strAction = empty($strAction) ? '' : $strAction . ' ';

            if (count($arrIDSuccess) > 0) {
                Yii::$app->session->addFlash('success',
                    $strAction . Yii::t(_NP_TEXT_CATE, 'Total {num} item(s) successfully affected: {items}.', [
                        'num' => 'count($arrIDSuccess)',
                        'items' => 'implode(\', \', $arrIDSuccess)',
                    ]));
            }

            if (count($arrIDFail) > 0) {
                Yii::$app->session->addFlash('error',
                    $strAction . Yii::t(_NP_TEXT_CATE, 'Total {num} item(s) failed: {items}.', [
                        'num' => 'count($arrIDFail)',
                        'items' => 'implode(\', \', $arrIDFail)',
                    ]));
            }

        } else {
            Yii::$app->session->addFlash('warning', Yii::t(_NP_TEXT_CATE, 'No items seleted'));
        }

        return $this->redirect($indexUrlQueryParams);
    }

    /**
     * Bulk action Active on selected items
     * @return \yii\web\Response
     */
    public function actionActive()
    {
        $action = Yii::$app->request->post('bulk-option');
        $selection = (array)Yii::$app->request->post('selection');

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        if (count($selection) > 0) {
            $arrIDSuccess = [];
            $arrIDFail = [];
            $arrSelection = explode(',', $selection[0]);
            foreach ($arrSelection as $id) {
                $result = false;
                if ($action == 'active') {
                    $strAction = Yii::t(_NP_TEXT_CATE, 'Execute Bulk Active.');
                    $user = $this->findModel($id);
                    $result = $user->setActiveUser();
                    if ($result == true) {
                        $strContent = NotifyMessage::getActiveTechnicianAccount();

                        $systemNotify = new SystemNotification();
                        $systemNotify->name = 'Admin active technician account';
                        $systemNotify->content = $strContent;
                        $systemNotify->receiver_emails = $user->email;
                        $systemNotify->is_emailed = 1;
                        $systemNotify->is_smsed = 0;
                        $systemNotify->email_template = 'systemNotificationDefaultMailTemplate';
                        $systemNotify->save();
                        $systemNotify->sendEmail();

                    }
                }
                if ($action == 'deny') {
                    $strAction = Yii::t(_NP_TEXT_CATE, 'Execute Bulk Deny.');
                    $user = $this->findModel($id);
                    $result = $user->setDenyUser();
                    if ($result == true) {
                        $strContent = NotifyMessage::getDenyTechnicianAccount();

                        $systemNotify = new SystemNotification();
                        $systemNotify->name = 'Admin deny technician register';
                        $systemNotify->content = $strContent;
                        $systemNotify->receiver_emails = $user->email;
                        $systemNotify->is_emailed = 1;
                        $systemNotify->is_smsed = 0;
                        $systemNotify->email_template = 'systemNotificationDefaultMailTemplate';
                        $systemNotify->save();
                        $systemNotify->sendEmail();

                    }

                }
                if ($result) {
                    ActivityLog::insertLogActivateUser(\Yii::$app->user->id, $id);
                    $arrIDSuccess[] = $id;
                } else {
                    $arrIDFail[] = $id;
                }
            }
            $strAction = empty($strAction) ? '' : $strAction . ' ';

            if (count($arrIDSuccess) > 0) {
                Yii::$app->session->addFlash('success',
                    $strAction . Yii::t(_NP_TEXT_CATE, 'Total {num} item(s) successfully affected: {items}.', [
                        'num' => count($arrIDSuccess),
                        'items' => implode(', ', $arrIDSuccess),
                    ]));
            }

            if (count($arrIDFail) > 0) {
                Yii::$app->session->addFlash('error',
                    $strAction . Yii::t(_NP_TEXT_CATE, 'Total {num} item(s) failed: {items}.', [
                        'num' => count($arrIDFail),
                        'items' => implode(', ', $arrIDFail),
                    ]));
            }

        } else {
            Yii::$app->session->addFlash('warning', Yii::t(_NP_TEXT_CATE, 'No items seleted'));
        }

        return $this->redirect(['technician-external-listing']);
    }

    public function actionListUserStandard()
    {
        $searchModel = new UserSearch();
        $searchModel->globalSearch = !empty($_REQUEST['q']) ? $_REQUEST['q'] : '';
        $dataProvider = $searchModel->searchStandard('');
        $page = !empty($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $dataProvider->pagination->page = $page < 1 ? 0 : $page - 1;

        /* @var $models User[] */
        $rows = User::getSelect2Data($dataProvider->models, false);
        $incomplete_results = !($dataProvider->pagination->page == $dataProvider->pagination->totalCount);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return [
                'items' => $rows,
                'total_count' => $dataProvider->totalCount,
                "incomplete_results" => $incomplete_results,
                "page" => $dataProvider->pagination->page
            ];
        }

        return Yii::$app->end();
    }

    public function actionListUserRelation($showAll = null)
    {
        $searchModel = new UserSearch();
        $searchModel->globalSearch = !empty($_REQUEST['q']) ? $_REQUEST['q'] : '';
        $dataProvider = $searchModel->search('');
        $page = !empty($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $dataProvider->pagination->page = $page < 1 ? 0 : $page - 1;

        /* @var $models User[] */
        $rows = User::getSelect2Data($dataProvider->models, false);

        $incomplete_results = !($dataProvider->pagination->page == $dataProvider->pagination->totalCount);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

            return [
                'items' => $rows,
                'total_count' => $dataProvider->totalCount,
                "incomplete_results" => $incomplete_results,
                "page" => $dataProvider->pagination->page
            ];
        }

        return Yii::$app->end();
    }

    public function actionChangePassword()
    {
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            ActivityLog::insertLogChangePassword(\Yii::$app->user->id);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Password has been changed.'));
            return $this->redirect(['change-password']);
        } else {
            return $this->render('change-password', [
                'model' => $model,
            ]);
        }
    }
}
