<?php

namespace backend\controllers;

use common\libs\ClsControllerBackend;
use Yii;
use common\models\PageItem;
use common\models\SearchPageItem;
use common\libs\ClsController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use enpii\enpiiCms\helpers\ArrayHelper;
use enpii\enpiiCms\helpers\DateTimeHelper;
use enpii\enpiiCms\helpers\CommonHelper;
use common\models\User;

/**
 * PageItemController implements the CRUD actions for PageItem model.
 */
class PageItemController extends ClsControllerBackend
{
    public function behaviors()
    {
        $parentOneBehaviors = parent::behaviors();
        $thisOneBehaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
        return ArrayHelper::merge($parentOneBehaviors, $thisOneBehaviors);
    }

    /**
     * Lists all PageItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchPageItem();
        if(empty(Yii::$app->getRequest()->getQueryParam('sort'))){
            $searchModel->sortDefault = 'created_at';
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PageItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Create a new PageItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PageItem();

        if ($model->load(Yii::$app->request->post())) {
            $model->is_deleted = 0;
            $model->creator_id = 1;
            $model->created_at = DateTimeHelper::toDbFormat();
            $model->updated_at = DateTimeHelper::toDbFormat();

            $model->status = User::_STATUS_PUBLISHED;
            if ($model->status == User::_STATUS_PUBLISHED) {
                $model->published_at = DateTimeHelper::toDbFormat();
            }

            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Data created successfully.'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Data created unsuccessfully. Please recheck the input.'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PageItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->updated_at = DateTimeHelper::toDbFormat();

            if ($model->status == PageItem::_STATUS_PUBLISHED) {
                $model->published_at = DateTimeHelper::toDbFormat();
            }
//            var_dump($model->slug);
//            var_dump($model->save());
//            var_dump($model->getErrors());
//            var_dump($model->attributes);
//            die();
            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Data updated successfully.'));
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Data updated unsuccessfully. Please recheck the input.'));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PageItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->putToTrash()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Data deleted successfully.'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Data deleted unsuccessfully.'));
        }

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        return $this->redirect($indexUrlQueryParams);
    }

    /**
     * Finds the PageItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested item does not exist.'));
        }
    }

    /**
     * Bulk action on selected items
     * @return \yii\web\Response
     */
    public function actionBulk()
    {
        $action = Yii::$app->request->post('bulk-option');
        $selection = (array)Yii::$app->request->post('selection');

        $queryParams = Yii::$app->request->getQueryParams();
        $indexUrlQueryParams = ArrayHelper::merge([0 => 'index'], $queryParams);

        if (count($selection) > 0) {
            $arrIDSuccess = [];
            $arrIDFail = [];

            foreach ($selection as $id) {
                $result = false;
                if ($action == 'delete') {
                    $strAction = Yii::t('app', 'Execute Bulk Deletion.');
                    $result = $this->findModel($id)->putToTrash();
                }
                if ($result) {
                    $arrIDSuccess[] = $id;
                } else {
                    $arrIDFail[] = $id;
                }
            }

            $strAction = empty($strAction) ? '' : $strAction . ' ';

            if (count($arrIDSuccess) > 0) {
                Yii::$app->session->addFlash('success',
                    $strAction . Yii::t('app', 'Total {num} item(s) successfully affected: {items}.', [
                        'num' => 'count($arrIDSuccess)',
                        'items' => 'implode(\', \', $arrIDSuccess)',
                    ]));
            }

            if (count($arrIDFail) > 0) {
                Yii::$app->session->addFlash('error', $strAction . Yii::t('app', 'Total {num} item(s) failed: {items}.', [
                        'num' => 'count($arrIDFail)',
                        'items' => 'implode(\', \', $arrIDFail)',
                    ]));
            }

        } else {
            Yii::$app->session->addFlash('warning', Yii::t('app', 'No items selected'));
        }

        return $this->redirect($indexUrlQueryParams);
    }
}
