<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\ContestSession;
use backend\models\ContestItem;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $totalETTicket  */
/* @var $totalETTicketClaimed */
/* @var $totalETTicketLeft */
/* @var $totalTATicket  */
/* @var $totalTATicketClaimed */
/* @var $totalTATicketLeft */

$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="user-index">
    <h3 class="page-title">
        <?= Html::encode($this->title) ?>
    </h3>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-ticket" aria-hidden="true"></i>
                </div>
                <div class="details">
                    <div class="number"><?= $totalETTicketClaimed ?>/<?= $totalETTicket ?></div>
                    <div class="desc"> ET Coupons Claimed </div>
                </div>
                <div class="more"> &nbsp;
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number"><?= $totalETTicketLeft ?>/<?= $totalETTicket ?></div>
                    <div class="desc">  ET Coupons Left </div>
                </div>
                <div class="more"> &nbsp;
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-ticket" aria-hidden="true"></i>
                </div>
                <div class="details">
                    <div class="number"><?= $totalTATicketClaimed ?>/<?= $totalTATicket ?></div>
                    <div class="desc">  TA Coupons Claimed </div>
                </div>
                <div class="more"> &nbsp;
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number"><?= $totalTATicketLeft ?>/<?= $totalTATicket ?></div>
                    <div class="desc">  TA Coupons Left </div>
                </div>
                <div class="more"> &nbsp;
                </div>
            </div>
        </div>

    </div>
</div>
