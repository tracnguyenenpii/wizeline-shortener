<?php
use yii\helpers\Html;
use enpii\enpiiCms\libs\gridview\NpGridView;
use enpii\enpiiCms\libs\NpActiveForm;
use enpii\enpiiCms\helpers\ArrayHelper;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $searchModel enpii\enpiiCms\models\UserRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var Yii::$app->request \enpii\enpiiCms\libs\NpRequest */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'User Role Management');
$this->setBrowserTitle($pageTitle, 0);
$this->params['breadcrumbs'][] = $pageTitle;

$queryParams = Yii::$app->request->getQueryParams();
$bulkQueryParams = ArrayHelper::merge([0 => 'bulk'], $queryParams);
?>
<div class="user-role-index">

    <h3 class="page-title">
        <?= Html::encode(Yii::t(_NP_TEXT_CATE, 'User Role Management')) ?>
    </h3>
    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green-sharp bold uppercase">
                                <?= Yii::t(_NP_TEXT_CATE, 'User Role Listing') ?>
                            </span>

                        </div>
                        <div class="actions">
                            <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">'.
                            Yii::t(_NP_TEXT_CATE, 'Create User Role'),
                            ['create'],
                            ['class' => 'btn blue btn-outline']) ?>
                        </div>
                    </div>

                    <?= $this->render('_search', ['model'  => $searchModel ])?>
                    <div class="portlet-body">
                    <?php                    // Hidden Form and link action's form must have save ID
                    $formID = 'bulk-actions';
                    $gridID = 'main-list';

                    $this->registerJs(<<<JSCODE
                        jQuery('input[name="selection[]"]').click(function(){
                            if (jQuery('#{$formID}').find('input[name="selection[]"]').length > 0) {
                                jQuery('#{$formID}').find('input[name="selection[]"]').val(jQuery('#main-list').yiiGridView('getSelectedRows'));
                            } else {
                                jQuery('#{$formID}').append($('<input/>').attr({type: 'hidden', name: 'selection[]', value: jQuery('#{$gridID}').yiiGridView('getSelectedRows')}));
                            }
                        });
JSCODE
                    );

                    // Put a hidden form here
                    NpActiveForm::begin(
                        [
                            'options' => [
                                'id' => $formID,
                            ],
                        ]
                    );
                    NpActiveForm::end();

                                        echo NpGridView::widget([
                        'id' => $gridID,
                        'tableOptions' => ['class' => 'kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap table-common-listing'],
                        'containerOptions'=>['style'=>'overflow: auto;'],
                        'headerRowOptions'=>['class'=>'table-heading'],
                        'filterRowOptions'=>['class'=>'table-filter'],
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hover' => true,
                        'pjax' => false, // pjax is set to always true for this demo
                        // set your toolbar
                        'toolbar' => [
                            [
                                'content' => Html::a('<i class="fa fa-repeat"></i> '. Yii::t(_NP_TEXT_CATE, 'Reset'), ['index'], [
                                    'data-pjax' => 0,
                                    'class' => 'btn btn-default',
                                    'title' => Yii::t(_NP_TEXT_CATE, 'Reset all params')])
                                . ' '
                                . Html::a('<i class="fa fa-trash"></i> '. Yii::t(_NP_TEXT_CATE, 'Delete'), $bulkQueryParams, [
                                     'class' => 'btn btn-danger',
                                     'title' => Yii::t(_NP_TEXT_CATE, 'Bulk Delete'),
                                     'data' => [
                                         'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure you want to delete?'),
                                         'form' => $formID,
                                         'method' => 'post',
                                         'params' => [
                                             'bulk-option' => 'delete',
                                         ],
                                     ],
                                ])
                            ],
                        ],
                        'panel' => [
                            'heading' => false,
                        ],
                        'resizableColumns' => true,
                        'resizeStorageKey' => 'user-role'.Yii::$app->user->id . '-' . date("d"),

                        'persistResize' => true,
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
'columns' => [
                            [
                                'class' => 'enpii\enpiiCms\libs\gridview\NpCheckBoxColumn',
                                'headerOptions' => [
                                    'class' => 'common-column-heading'
                                ],
                                'checkboxOptions' => ['class' => 'default-checkbox'],
                            ],                             [
                                'class' => 'enpii\enpiiCms\libs\gridview\NpSerialColumn',
                                'contentOptions' => ['class' => 'serial-column'],
                                'width' => '1%',
                                'vAlign' => 'middle',
                                'hAlign' => 'right',
                            ], 
                                        'id',
            'name',
            'code',
            'description:ntext',
            'parent_id',
            // 'level',

                            ['class' => 'enpii\\enpiiCms\\libs\\gridview\\NpActionColumn',
                                'viewOptions' => [
                                    'title' => Yii::t(_NP_TEXT_CATE, 'View this item'),
                                    'data-original-title' => Yii::t(_NP_TEXT_CATE, 'View this item'),
                                    'data-container' => 'body',
                                    'class' => 'tooltips',
                                    'data-toggle' => 'tooltip'
                                ],
                                'updateOptions' => [
                                    'title' => Yii::t(_NP_TEXT_CATE, 'Edit this item'),
                                    'data-original-title' => Yii::t(_NP_TEXT_CATE, 'Edit this item'),
                                    'data-container' => 'body',
                                    'class' => 'tooltips',
                                    'data-toggle' => 'tooltip'
                                ],
                                'deleteOptions' => [
                                    'title' => Yii::t(_NP_TEXT_CATE, 'Delete this item'),
                                    'data-original-title' => Yii::t(_NP_TEXT_CATE, 'Delete this item'),
                                    'data-container' => 'body',
                                    'class' => 'tooltips',
                                    'data-toggle' => 'tooltip'
                                ],
                                'headerOptions' => ['class' => 'np-action-column'],
                            ],
                        ]
                    ]);
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
