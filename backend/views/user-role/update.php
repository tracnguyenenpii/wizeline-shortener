<?php

use yii\helpers\Html;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model enpii\enpiiCms\models\UserRole */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'Update {modelClass}', [
    'modelClass' => Yii::t(_NP_TEXT_CATE, 'User Role'),
]) . ' ID: ' . $model->name;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t(_NP_TEXT_CATE, 'User Role Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t(_NP_TEXT_CATE, 'Update');

?>
<div class="user-role-update">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t(_NP_TEXT_CATE, 'User Role Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
