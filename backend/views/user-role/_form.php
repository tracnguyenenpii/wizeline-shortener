<?php

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model enpii\enpiiCms\models\UserRole */
/* @var $form enpii\enpiiCms\libs\NpActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-green-sharp bold uppercase">
                    <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="user-role-form">

                    <?php $form = NpActiveForm::begin(['options' => ['class'=> 'single-form', 'role' => 'form']]); ?>

                    <div class="form-body">

                    <?= $form->fieldMaterialDesign($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->fieldMaterialDesign($model, 'code')->textInput(['maxlength' => true]) ?>
    <?= $form->fieldMaterialDesign($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->fieldMaterialDesign($model, 'description')->textarea(['rows' => 6]) ?>

                    </div>

                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t(_NP_TEXT_CATE, 'Create') : Yii::t(_NP_TEXT_CATE, 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php NpActiveForm::end(); ?>

                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
