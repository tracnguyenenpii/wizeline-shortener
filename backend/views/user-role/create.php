<?php

use yii\helpers\Html;


/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model enpii\enpiiCms\models\UserRole */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'Create User Role');
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t(_NP_TEXT_CATE, 'User Role Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageTitle;

?>
<div class="user-role-create">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t(_NP_TEXT_CATE, 'User Role Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
