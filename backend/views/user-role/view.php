<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model enpii\enpiiCms\models\UserRole */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'View {modelClass}', [
    'modelClass' => Yii::t(_NP_TEXT_CATE, 'User Role'),
]) . ' ID: ' . $model->name;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t(_NP_TEXT_CATE, 'User Role Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t(_NP_TEXT_CATE, 'View');

?>
<div class="user-role-view">

    <h3 class="page-title"><?=  Html::encode(Yii::t(_NP_TEXT_CATE, 'User Role Management')) ?></h3>

    <div class="caption font-green-sharp bold uppercase">
        <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
    </div>

    <p>
        <?= Html::a(Yii::t(_NP_TEXT_CATE, 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t(_NP_TEXT_CATE, 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure you want to delete?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">'.
            Yii::t(_NP_TEXT_CATE, 'Create User Role'),
            ['create'],
            ['class' => 'btn blue btn-outline pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'code',
            'description:ntext',
            'parent_id',
            'level',
        ],
    ]) ?>

</div>
