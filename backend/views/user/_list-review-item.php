<?php
/**
 * Created by PhpStorm.
 * User: lacphan
 * Date: 9/27/16
 * Time: 5:31
 * @var $model \common\models\OsrUserReview
 * @var $osrReviewProvider \common\models\OsrUserReviewSearch
 * @var $userRole
 */
if ($userRole == \common\models\User::_TECH_INTERNAL_CODE || $userRole == \common\models\User::_TECH_EXTERNAL_CODE) {
    $user = $model->getUser();
} else {
    $user = $model->getTechnician();
}
//\enpii\enpiiCms\helpers\CommonHelper::debugVar($user, true);
?>
<?php
if ($user) {
    ?>
    <div class="post-item">
        <div class="pull-left image avatar">

            <?php if (!empty($user->avatar_id)): ?>
                <?php
                $image = $user->getAvatarAttachments();
                ?>
                <img src="<?= $image[0]['thumbnailUrl'] ?>">
            <?php else: ?>
                <img src="<?= APP_BASE_URL . '/assets-enpii/image/user_default.jpg' ?>"
                     class="img-circle avatar"
                     alt="user profile image">
            <?php endif; ?>

        </div>
        <div class="pull-left meta">
            <div class="title h5">
                <?php if ($userRole == \common\models\User::_TECH_INTERNAL_CODE || $userRole == \common\models\User::_TECH_EXTERNAL_CODE): ?>
                    <a><b><?= $user->getFullName() . ' ' . '(' . $user->email . ')' ?></b></a>
                <?php else: ?>
                    <a><b><?= $user->getFullName() . ' ' . '(' . $user->email . ')' ?></b></a>
                <?php endif; ?>

            </div>
            <p><i><?= $model->review ?></i></p>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php
}
?>
