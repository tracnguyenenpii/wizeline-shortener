<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/9/16 2:17 PM
 */
/* @var $this \enpii\enpiiCms\libs\NpView */
/* @var $form \enpii\enpiiCms\libs\NpActiveForm */
/* @var $pendingOSR int */
/* @var $depositedOSR int */
/* @var $externalPending int */
/* @var $internalTotal int */

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;


$pageTitle = Yii::t(_NP_TEXT_CATE, 'Dashboard');
$this->setBrowserTitle($pageTitle, 0);
$this->params['breadcrumbs'][] = $pageTitle;
?>

<!-- BEGIN PAGE TITLE-->
<h3 class="page-title"> <?= $pageTitle ?>
    <small><?= Yii::t(_NP_TEXT_CATE, 'Showing site stats') ?></small>
</h3>
<!-- END PAGE TITLE-->

<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">

</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->

<div class="row">
    <div class="col-md-12 col-sm-12">


    </div>
</div>

<div style="height: 1500px;"></div>
