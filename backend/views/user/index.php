<?php
use yii\helpers\Html;
use enpii\enpiiCms\libs\gridview\NpGridView;
use enpii\enpiiCms\libs\NpActiveForm;
use enpii\enpiiCms\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\export\ExportMenu;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $searchModel enpii\enpiiCms\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var Yii::$app->request \enpii\enpiiCms\libs\NpRequest */
/* @var $bulkAction boolean */
/* @var $role string */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'User Management');
$this->setBrowserTitle($pageTitle, 0);
$this->params['breadcrumbs'][] = $pageTitle;

$queryParams = Yii::$app->request->getQueryParams();
$bulkQueryParams = ArrayHelper::merge([0 => 'bulk'], $queryParams);

?>
<div class="user-index">

    <h3 class="page-title">
        <?= Html::encode(Yii::t(_NP_TEXT_CATE, 'User Management')) ?>
    </h3>
    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green-sharp bold uppercase">
                                <?= Yii::t(_NP_TEXT_CATE, 'User Listing') ?>
                            </span>

                        </div>
                        <div class="actions">
                            <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">' .
                                Yii::t(_NP_TEXT_CATE, 'Create User'),
                                ['create'],
                                ['class' => 'btn blue btn-outline']) ?>
                        </div>
                    </div>

                    <?= $this->render('_search', ['model' => $searchModel]) ?>
                    <div class="portlet-body">
                        <?php // Hidden Form and link action's form must have save ID
                        $formID = 'bulk-actions';
                        $gridID = 'main-list';

                        $this->registerJs(<<<JSCODE
                        jQuery('input[name="selection[]"]').click(function(){
                            if (jQuery('#{$formID}').find('input[name="selection[]"]').length > 0) {
                                jQuery('#{$formID}').find('input[name="selection[]"]').val(jQuery('#main-list').yiiGridView('getSelectedRows'));
                            } else {
                                jQuery('#{$formID}').append($('<input/>').attr({type: 'hidden', name: 'selection[]', value: jQuery('#{$gridID}').yiiGridView('getSelectedRows')}));
                            }
                        });
JSCODE
                        );

                        // Put a hidden form here
                        NpActiveForm::begin(
                            [
                                'options' => [
                                    'id' => $formID,
                                ],
                            ]
                        );
                        NpActiveForm::end();
                        if ($role == 'user') {
                            echo ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    'email',
                                    'username',
                                    'first_name',
                                    'last_name',
                                    'mobile_phone',
                                    'billing_address',
                                    'billing_address2',
                                    [
                                        'label' => 'Billing Country',
                                        'value' => 'countryText.name',
                                    ],
                                    [
                                        'label' => 'Billing Country',
                                        'value' => 'cityText.name',
                                    ],
                                    'billing_post_code',
                                    'service_address',
                                    'service_address2',
                                    [
                                        'label' => 'Service Country',
                                        'value' => 'serviceCountryText.name',
                                    ],
                                    [
                                        'label' => 'Service City',
                                        'value' => 'serviceCityText.name',
                                    ],

                                    'service_post_code',
                                    [
                                        'label' => 'Register Date',
                                        'value' => 'created_at'
                                    ]

                                ],
                                'exportConfig' => [
                                    ExportMenu::FORMAT_HTML => false,
                                    ExportMenu::FORMAT_CSV => false,
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_PDF => false,
                                ]
                            ]);
                        } else {
                            echo ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    'email',
                                    'username',
                                    'first_name',
                                    'last_name',
                                    'mobile_phone',
                                    'work_permit_number',
                                    'postal_code',
                                    [
                                        'label' => 'Category',
                                        'value' => 'categoryText'
                                    ],
                                    [
                                        'label' => 'Service Type',
                                        'value' => 'serviceTypeText'
                                    ],
                                    [
                                        'label' => 'Preferred Location',
                                        'value' => 'preferredLocationText'
                                    ],
                                    'qualification_level',
                                    'qualification_type',
                                    'experience',
                                    [
                                        'label' => 'Register Date',
                                        'value' => 'created_at'
                                    ]

                                ],
                                'exportConfig' => [
                                    ExportMenu::FORMAT_HTML => false,
                                    ExportMenu::FORMAT_CSV => false,
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_PDF => false,
                                ]
                            ]);
                        }
                        $arrOption = [
                            'id' => $gridID,
                            'tableOptions' => ['class' => 'kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap table-common-listing'],
                            'containerOptions' => ['style' => 'overflow: auto;'],
                            'headerRowOptions' => ['class' => 'table-heading'],
                            'filterRowOptions' => ['class' => 'table-filter'],
                            'bordered' => true,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'hover' => true,
                            'pjax' => false, // pjax is set to always true for this demo
                            // set your toolbar

                            'panel' => [
                                'heading' => false,
                            ],
                            'resizableColumns' => true,
                            'resizeStorageKey' => 'user' . Yii::$app->user->id . '-' . date("d"),

                            'persistResize' => true,
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                [
                                    'class' => 'enpii\enpiiCms\libs\gridview\NpCheckBoxColumn',
                                    'headerOptions' => [
                                        'class' => 'common-column-heading'
                                    ],
                                    'checkboxOptions' => ['class' => 'default-checkbox'],
                                ], [
                                    'class' => 'enpii\enpiiCms\libs\gridview\NpSerialColumn',
                                    'contentOptions' => ['class' => 'serial-column'],
                                    'width' => '1%',
                                    'vAlign' => 'middle',
                                    'hAlign' => 'right',
                                ],
//                                'id',
                                'username',
                                'email:email',
                                'mobile_phone',
                                'first_name',
                                // 'middle_name',
                                'last_name',
//                                [
//                                    'attribute' => 'user_role_ids',
//                                    'value' => function ($model) {
//                                        $arrRoles = $model->userRoles;
//                                        return implode(ArrayHelper::map($arrRoles, 'id', 'name'));
//                                    },
//                                    'filter' => \enpii\enpiiCms\widgets\DataRelationInput::widget([
//                                        'model' => $searchModel,
//                                        'attribute' => 'user_role_ids',
//                                        'data' => ArrayHelper::map(\enpii\enpiiCms\models\UserRole::find()->asArray()->all(), 'id', 'name'),
//                                        'theme' => Select2::THEME_BOOTSTRAP,
//                                        'hideSearch' => false,
//                                        'options' => [
//                                            'placeholder' => Yii::t(_NP_TEXT_CATE, 'Select Roles'),
//                                                'multiple' => false,
//                                        ]
//                                    ]),
//                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function ($model) {
                                        $statuses = \common\models\User::getStatuses();
                                        if (array_key_exists($model->status, $statuses)) {
                                            return $statuses[$model->status];
                                        }
                                        return null;
                                    },
                                    'filter' => \enpii\enpiiCms\widgets\DataRelationInput::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'status',
                                        'data' => \common\models\User::getStatuses(),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'hideSearch' => false,
                                        'options' => [
                                            'placeholder' => Yii::t(_NP_TEXT_CATE, 'Select Status'),
                                            'multiple' => false,
                                        ]
                                    ]),
                                ],
                                // 'params:ntext',

                                ['class' => 'enpii\\enpiiCms\\libs\\gridview\\NpActionColumn',
                                    'viewOptions' => [
                                        'title' => Yii::t(_NP_TEXT_CATE, 'View this item'),
                                        'data-original-title' => Yii::t(_NP_TEXT_CATE, 'View this item'),
                                        'data-container' => 'body',
                                        'class' => 'tooltips',
                                        'data-toggle' => 'tooltip'
                                    ],
                                    'updateOptions' => [
                                        'title' => Yii::t(_NP_TEXT_CATE, 'Edit this item'),
                                        'data-original-title' => Yii::t(_NP_TEXT_CATE, 'Edit this item'),
                                        'data-container' => 'body',
                                        'class' => 'tooltips',
                                        'data-toggle' => 'tooltip'
                                    ],
                                    'deleteOptions' => [
                                        'title' => Yii::t(_NP_TEXT_CATE, 'Delete this item'),
                                        'data-original-title' => Yii::t(_NP_TEXT_CATE, 'Delete this item'),
                                        'data-container' => 'body',
                                        'class' => 'tooltips',
                                        'data-toggle' => 'tooltip'
                                    ],
                                    'headerOptions' => ['class' => 'np-action-column'],
                                ],
                            ]
                        ];
                        if ($bulkAction == true) {
                            $arrOption['toolbar'] = [
                                [
                                    'content' => Html::a('<i class="fa fa-repeat"></i> ' . Yii::t(_NP_TEXT_CATE, 'Reset'), [Yii::$app->controller->action->id], [
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-default',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Reset all params')])
                                        . ' '
                                        . Html::a('<i class="fa fa-check"></i> ' . Yii::t(_NP_TEXT_CATE, 'Activate'), ['active'], [
                                            'data-pjax' => 0,
                                            'class' => 'btn green-haze',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Activate User'),
                                            'data' => [
                                                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure want to activate this users?'),
                                                'form' => $formID,
                                                'method' => 'post',
                                                'params' => [
                                                    'bulk-option' => 'active',
                                                ],
                                            ],
                                        ])
                                        . Html::a('<i class="fa fa-ban"></i> ' . Yii::t(_NP_TEXT_CATE, 'Deactivate'), ['active'], [
                                            'data-pjax' => 0,
                                            'class' => 'btn grey-mint',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Deactivate User'),
                                            'data' => [
                                                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure want to deactivate this users?'),
                                                'form' => $formID,
                                                'method' => 'post',
                                                'params' => [
                                                    'bulk-option' => 'deny',
                                                ],
                                            ],
                                        ])
                                        . Html::a('<i class="fa fa-trash"></i> ' . Yii::t(_NP_TEXT_CATE, 'Delete'), $bulkQueryParams, [
                                            'class' => 'btn btn-danger',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Bulk Delete'),
                                            'data' => [
                                                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure you want to delete?'),
                                                'form' => $formID,
                                                'method' => 'post',
                                                'params' => [
                                                    'bulk-option' => 'delete',
                                                ],
                                            ],
                                        ])
                                ],
                            ];
                        } else {
                            $arrOption['toolbar'] = [
                                [
                                    'content' => Html::a('<i class="fa fa-repeat"></i> ' . Yii::t(_NP_TEXT_CATE, 'Reset'), [Yii::$app->controller->action->id], [
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-default',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Reset all params')])
                                        . ' '
                                        . Html::a('<i class="fa fa-trash"></i> ' . Yii::t(_NP_TEXT_CATE, 'Delete'), $bulkQueryParams, [
                                            'class' => 'btn btn-danger',
                                            'title' => Yii::t(_NP_TEXT_CATE, 'Bulk Delete'),
                                            'data' => [
                                                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure you want to delete?'),
                                                'form' => $formID,
                                                'method' => 'post',
                                                'params' => [
                                                    'bulk-option' => 'delete',
                                                ],
                                            ],
                                        ])
                                ],
                            ];
                        }
                        echo NpGridView::widget($arrOption);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
