<?php
/**
 * Created by PhpStorm.
 * User: quocanhnguyen
 * Date: 8/18/16
 * Time: 5:33 PM
 */
use enpii\enpiiCms\libs\NpActiveForm;
use yii\helpers\Html;

/* @var $this \enpii\enpiiCms\libs\NpView */
/* @var $model \enpii\enpiiCms\libs\NpModel */


$pageTitle = Yii::t(_NP_TEXT_CATE, 'Change Password');
$this->setBrowserTitle($pageTitle, 0);
$this->params['breadcrumbs'][] = $pageTitle;

?>
<div class="user-index">

    <h3 class="page-title">
        <?= Html::encode($pageTitle) ?>
    </h3>
    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">

                    <div class="portlet-body">
                        <?php $form = NpActiveForm::begin(); ?>
                        <?= $form->fieldMaterialDesign($model, 'oldPassword')->passwordInput()->label('Old Password') ?>
                        <?= $form->fieldMaterialDesign($model, 'newPassword')->passwordInput()->label('New Password') ?>
                        <?= $form->fieldMaterialDesign($model, 'rePassword')->passwordInput()->label('Confirm Password') ?>
                        <div class="form-submit">
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?php NpActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

