<?php

use yii\helpers\Html;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model enpii\enpiiCms\models\User */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'Update {modelClass}', [
    'modelClass' => Yii::t(_NP_TEXT_CATE, 'User'),
]) . ' ID: ' . $model->id;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t(_NP_TEXT_CATE, 'User Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t(_NP_TEXT_CATE, 'Update');

?>
<div class="user-update">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t(_NP_TEXT_CATE, 'User Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
