<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\User */

$pageTitle = Yii::t(_NP_TEXT_CATE, 'View {modelClass}', [
    'modelClass' => Yii::t(_NP_TEXT_CATE, 'User'),
]) . ' ID: ' . $model->id;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t(_NP_TEXT_CATE, 'User Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t(_NP_TEXT_CATE, 'View');

?>
<div class="user-view">

    <h3 class="page-title"><?=  Html::encode(Yii::t(_NP_TEXT_CATE, 'User Management')) ?></h3>

    <div class="caption font-green-sharp bold uppercase">
        <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
    </div>

    <p>
        <?= Html::a(Yii::t(_NP_TEXT_CATE, 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t(_NP_TEXT_CATE, 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t(_NP_TEXT_CATE, 'Are you sure you want to delete?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">'.
            Yii::t(_NP_TEXT_CATE, 'Create User'),
            ['create'],
            ['class' => 'btn blue btn-outline pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'phone',
            'first_name',
            'last_name',
            [
                'attribute' => 'created_at',
                'format'=>'raw',
                'value'=> $model->getCreatedAt(),
            ],
            [
                'attribute' => 'updated_at',
                'format'=>'raw',
                'value'=> $model->getUpdatedAt(),
            ],
            [
                'attribute' => 'published_at',
                'format'=>'raw',
                'value'=> $model->getPublishedAt(),
            ],
        ],
    ]) ?>
</div>
<?php

$script = <<<JS
$('#review-rating-disable').barrating({
    theme: 'fontawesome-stars-o',
    readonly: true,
    deselectable: false,
    showSelectedRating: false,
    initialRating: $('#review-rating-disable').data('current-rating'),
});
JS;

$this->registerJs($script);
