<?php

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;
//use kartik\datecontrol\DateControl;
use enpii\enpiiCms\widgets\DataRelationInput;
use dosamigos\fileupload\FileUploadUI;
use enpii\enpiiCms\models\User;
use enpii\enpiiCms\widgets\attachmentInput\AttachmentInputUI;
use enpii\enpiiCms\helpers\StringHelper;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\User */
/* @var $form enpii\enpiiCms\libs\NpActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-green-sharp bold uppercase">
                    <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="user-form">

                    <?php $form = NpActiveForm::begin([
                        'id' => 'user-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'options' => [
                            'class' => 'single-form',
                            'role' => 'form'
                        ]
                    ]); ?>

                    <div class="form-body">

                        <!--                        --><? //= $form->fieldMaterialDesign($model, 'username')->textInput(['maxlength' => true]) ?>

                        <?= $form->fieldMaterialDesign($model, 'email')->textInput(['maxlength' => true]) ?>

                        <?= $form->fieldMaterialDesign($model, 'password')->passwordInput(['maxlength' => true]) ?>

                        <?= $form->fieldMaterialDesign($model, 'password_confirm')->passwordInput(['maxlength' => true]) ?>


                        <?php
                        $urlUserRole = Yii::$app->urlManager->createAbsoluteUrl(['enpiiCms/backend/user-role/index', 'layout' => 'backend/relation']);
                        ?>

                        <?php
                        $urlRelationList = Yii::$app->urlManager->createAbsoluteUrl(['enpiiCms/backend/user-role/list-relation']);
                        $urlView = Yii::$app->urlManager->createAbsoluteUrl(['enpiiCms/backend/user-role/view', 'id' => '--id--', 'layout' => 'backend/simple']);
                        $urlAdd = Yii::$app->urlManager->createAbsoluteUrl(['enpiiCms/backend/user-role/create', 'layout' => 'backend/simple']);


                        $config['targetClass'] = 'enpii\enpiiCms\models\UserRole';
                        $config['urlRelationList'] = $urlRelationList;
                        $config['urlView'] = $urlView;
                        $config['textPlaceholder'] = Yii::t(_NP_TEXT_CATE, 'Select a {name}', ['name' => Yii::t(_NP_TEXT_CATE, 'User Role')]) . ' ...';
                        $config['minimumInputLength'] = 0;
                        $config['maximumSelectionLength'] = 3;

                        echo $form->fieldMaterialDesign($model, 'user_role_ids')->widget(DataRelationInput::className(), $config);

                        echo '<div class="relation-add-more"><a href="' . $urlAdd . '" class="btn btn-primary colorbox iframe" >' . '<i class="fa fa-plus"></i> ' . Yii::t(_NP_TEXT_CATE, "Add more {name}", ['name' => Yii::t(_NP_TEXT_CATE, 'User Role')]) . '</a></div>';

                        ?>

                        <?= $form->fieldMaterialDesign($model, 'first_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->fieldMaterialDesign($model, 'last_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->fieldMaterialDesign($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>

                        <?php
                        $preload = $model->isNewRecord ? false : true;
                        echo $form->fieldMaterialDesign($model, 'avatar', ['options' => ['class' => 'blueimp-file-upload']])->widget(AttachmentInputUI::className(), [
                            'url' => ['handle-upload', 'model_class' => User::className(), 'id' => $model->id],
                            'formId' => $form->id,
                            'load' => $preload,
                            'gallery' => false,
                            'fieldOptions' => [
                                'accept' => 'image/*',
                            ],
                            'clientOptions' => [
                                'maxChunkSize' => 5 * 100 * 1024,
                                'maxFileSize' => 5 * 1024 * 1024,
                                'autoUpload' => true,
                                'multiple' => false,
                                'maxNumberOfFiles' => 1,
                                'messages' => [
                                    'maxNumberOfFiles' => 'Only one Avatar Image is allowed. Please delete existing file in order to upload a new file.',
                                    'acceptFileTypes' => 'File type not allowed',
                                    'maxFileSize' => 'File is too large',
                                    'minFileSize' => 'File is too small'
                                ],
                            ],
                        ]);
                        ?>

                    </div>

                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t(_NP_TEXT_CATE, 'Create') : Yii::t(_NP_TEXT_CATE, 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php NpActiveForm::end(); ?>

                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
