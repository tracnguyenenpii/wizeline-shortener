<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\CouponPin */

$pageTitle = Yii::t('app', 'View {modelClass}', [
    'modelClass' => Yii::t('app', 'Coupon Pin'),
]) . ' ID: ' . $model->id;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coupon Pin Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>
<div class="coupon-pin-view">

    <h3 class="page-title"><?=  Html::encode(Yii::t('app', 'Coupon Pin Management')) ?></h3>

    <div class="caption font-green-sharp bold uppercase">
        <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">'.
            Yii::t('app', 'Create Coupon Pin'),
            ['create'],
            ['class' => 'btn blue btn-outline pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pin',
            'http_referrer',
            'user_browser_agent',
            'user_email:email',
            'user_ip',
            'user_session',
            'notes:ntext',
            'obtained_at',
            'is_obtained',
        ],
    ]) ?>

</div>
