<?php
use yii\helpers\Html;
use enpii\enpiiCms\libs\gridview\NpGridView;
use enpii\enpiiCms\libs\NpActiveForm;
use enpii\enpiiCms\helpers\ArrayHelper;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $searchModel common\models\CouponPinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var Yii::$app->request \enpii\enpiiCms\libs\NpRequest */

$pageTitle = Yii::t('app', 'Coupon Pin Management');
$this->setBrowserTitle($pageTitle, 0);
$this->params['breadcrumbs'][] = $pageTitle;

$queryParams = Yii::$app->request->getQueryParams();
$bulkQueryParams = ArrayHelper::merge([0 => 'bulk'], $queryParams);
?>
<div class="coupon-pin-index">

    <h3 class="page-title">
        <?= Html::encode(Yii::t('app', 'Coupon Pin Management')) ?>
    </h3>
    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green-sharp bold uppercase">
                                <?= Yii::t('app', 'Coupon Pin Listing') ?>
                            </span>

                        </div>
                        <div class="actions">
                            <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">' .
                                Yii::t('app', 'Create Coupon Pin'),
                                ['create'],
                                ['class' => 'btn blue btn-outline']) ?>
                        </div>
                    </div>

                    <?= $this->render('_search', ['model' => $searchModel]) ?>
                    <div class="portlet-body">
                        <?php // Hidden Form and link action's form must have save ID
                        $formID = 'bulk-actions';
                        $gridID = 'main-list';

                        $this->registerJs(<<<JSCODE
                        jQuery('input[name="selection[]"]').click(function(){
                            if (jQuery('#{$formID}').find('input[name="selection[]"]').length > 0) {
                                jQuery('#{$formID}').find('input[name="selection[]"]').val(jQuery('#main-list').yiiGridView('getSelectedRows'));
                            } else {
                                jQuery('#{$formID}').append($('<input/>').attr({type: 'hidden', name: 'selection[]', value: jQuery('#{$gridID}').yiiGridView('getSelectedRows')}));
                            }
                        });
JSCODE
                        );

                        // Put a hidden form here
                        NpActiveForm::begin(
                            [
                                'options' => [
                                    'id' => $formID,
                                ],
                            ]
                        );
                        NpActiveForm::end();

                        echo NpGridView::widget([
                            'id' => $gridID,
                            'tableOptions' => ['class' => 'kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap table-common-listing'],
                            'containerOptions' => ['style' => 'overflow: auto;'],
                            'headerRowOptions' => ['class' => 'table-heading'],
                            'filterRowOptions' => ['class' => 'table-filter'],
                            'bordered' => true,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'hover' => true,
                            'pjax' => false, // pjax is set to always true for this demo
                            // set your toolbar
                            'toolbar' => [
                                [
                                    'content' =>
                                        Html::a('<i class="fa fa-repeat"></i>  ' . Yii::t('app', 'Reset coupon'), $bulkQueryParams, [
                                            'class' => 'btn green-haze',
                                            'title' => Yii::t('app', 'Reset Coupon'),
                                            'data' => [
                                                'confirm' => Yii::t('app', 'Are you sure you want to do this?'),
                                                'form' => $formID,
                                                'method' => 'post',
                                                'params' => [
                                                    'bulk-option' => 'reset-coupon',
                                                ],
                                            ],
                                        ])
                                        . Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reset Filter'), ['index'], [
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-default',
                                            'title' => Yii::t('app', 'Reset all params')])
                                        

                                ],
                            ],
                            'panel' => [
                                'heading' => false,
                            ],
                            'resizableColumns' => true,
                            'resizeStorageKey' => 'coupon-pin' . Yii::$app->user->id . '-' . date("d"),

                            'persistResize' => true,
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                [
                                    'class' => 'enpii\enpiiCms\libs\gridview\NpCheckBoxColumn',
                                    'headerOptions' => [
                                        'class' => 'common-column-heading'
                                    ],
                                    'checkboxOptions' => ['class' => 'default-checkbox'],
                                ], [
                                    'class' => 'enpii\enpiiCms\libs\gridview\NpSerialColumn',
                                    'contentOptions' => ['class' => 'serial-column'],
                                    'width' => '1%',
                                    'vAlign' => 'middle',
                                    'hAlign' => 'right',
                                ],
                                'id',
                                'pin',
                                'user_ip',
                                'user_browser_agent',
                                [
                                    'attribute' => 'is_obtained',
                                    'label' => 'Obtained',
                                    'value' => function ($model) {
                                        /**
                                         * @var $model \common\models\CouponPin
                                         */
                                        if ($model->is_obtained == 0) {
                                            return Yii::t('app', 'None');
                                        }
                                        return Yii::t('app', 'Used');
                                    },
                                    'filter' => \kartik\select2\Select2::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'is_obtained',
                                        'data' => [
                                            0 => Yii::t('app', 'None'),
                                            1 => Yii::t('app', 'Used'),
                                        ],
                                        'theme' => \kartik\select2\Select2::THEME_BOOTSTRAP,
                                        'hideSearch' => true,
                                    ]),
                                ],
                                [
                                    'attribute' => 'obtained_at',
                                    'value' => function ($model) {
                                        /**
                                         * @var $model \common\models\CouponPin
                                         */
                                        if ($model->obtained_at === '0000-00-00 00:00:00') {
                                            return '';
                                        }
                                        return \enpii\enpiiCms\helpers\DateTimeHelper::fromDbFormat($model->obtained_at, 'Y-m-d H:i:s', Yii::$app->formatter->timeZone);
                                    },
                                ],
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
