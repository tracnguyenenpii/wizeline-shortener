<?php

use yii\helpers\Html;


/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\CouponPin */

$pageTitle = Yii::t('app', 'Create Coupon Pin');
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coupon Pin Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageTitle;

?>
<div class="coupon-pin-create">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t('app', 'Coupon Pin Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
