<?php

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ContentBlockItem */
/* @var $form enpii\enpiiCms\libs\NpActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-green-sharp bold uppercase">
                    <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="content-block-item-form">

                    <?php $form = NpActiveForm::begin(['options' => ['class' => 'single-form', 'role' => 'form']]); ?>

                    <div class="form-body">

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'content')->widget(TinyMce::className(), [
                            'options' => ['rows' => 20],
                            'language' => 'en_CA',
                            'clientOptions' => [
                                'fontSize' => 30,
                                'convert_urls' => false,
                                'relative_urls' => false,
                                'plugins' => [
                                    "advlist autolink lists link charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste image"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent | link image",
                                'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) { 
                                if(type=='image') $('.insert-file').click();
        }"),
                            ]
                        ]); ?>
                        <iframe id="form_target" name="form_target" style="display:none">
                            <form id="my_form" action="/upload/" target="form_target" method="post"
                                  enctype="multipart/form-data">
                                <input class="insert-file" name="image" type="file"
                                       onchange="$('#my_form').submit();this.value='';">
                            </form>
                        </iframe>

<!--                        --><?//= $form->field($model, 'created_at')->textInput() ?>

                    </div>

                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    <?php NpActiveForm::end(); ?>
                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
