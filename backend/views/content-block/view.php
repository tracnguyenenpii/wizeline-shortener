<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ContentBlockItem */

$pageTitle = Yii::t('app', 'View {modelClass}', [
        'modelClass' => Yii::t('app', 'Content Block Item'),
    ]) . ' ID: ' . $model->name;
$this->setBrowserTitle($pageTitle);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Block Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>
<div class="content-block-item-view">

    <h3 class="page-title"><?= Html::encode(Yii::t('app', 'Content Block Item Management')) ?></h3>

    <div class="caption font-green-sharp bold uppercase">
        <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('<i class="fa fa-plus"></i> <span class="hidden-480">' .
            Yii::t('app', 'Create Content Block Item'),
            ['create'],
            ['class' => 'btn blue btn-outline pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
//            'slug',
//            'locale_id',
            [
                'attribute' => 'content',
                'format' => 'raw'
            ],
            'created_at',
//            'updated_at',
//            'published_at',
//            'creator_id',
//            'is_deleted',
//            'ordering_weight',
//            'status',
//            'params:ntext',
//            'code',
        ],
    ]) ?>

</div>
