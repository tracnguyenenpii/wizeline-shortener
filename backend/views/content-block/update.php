<?php

use yii\helpers\Html;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ContentBlockItem */

$pageTitle = Yii::t('app', 'Update {modelClass}', [
    'modelClass' => 'Yii::t(\'app\', \'Content Block Item\')',
]) . ' ID: ' . $model->name;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Block Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="content-block-item-update">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t('app', 'Content Block Item Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
