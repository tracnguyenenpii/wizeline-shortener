<?php

use yii\helpers\Html;


/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\ContentBlockItem */

$pageTitle = Yii::t('app', 'Create Content Block Item');
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Block Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageTitle;

?>
<div class="content-block-item-create">

    <h3 class="page-title">
        <?=  Html::encode(Yii::t('app', 'Content Block Item Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
