<?php

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\PageItem */
/* @var $form enpii\enpiiCms\libs\NpActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-green-sharp bold uppercase">
                    <span class="caption-subject bold uppercase"><?= Html::encode($pageTitle) ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="page-item-form">

                    <?php $form = NpActiveForm::begin(['options' => ['class' => 'single-form', 'role' => 'form']]); ?>

                    <div class="form-body">

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

                        <!--                        --><? //= $form->field($model, 'locale_id')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className(), [
                            'clientOptions' => [
                                'imageManagerJson' => ['/redactor/upload/image-json'],
                                'imageUpload' => ['/redactor/upload/image'],
                                'fileUpload' => ['/redactor/upload/file'],
                                'lang' => 'en',
                                'plugins' => ['inlinestyle', 'source', 'codemirror','alignment', 'table', 'textexpander', 'fontcolor','imagemanager']
                            ]
                        ])?>

                    </div>

                    <div class="form-actions">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php NpActiveForm::end(); ?>

                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
