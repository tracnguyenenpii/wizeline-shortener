<?php

use yii\helpers\Html;

/* @var $this enpii\enpiiCms\libs\NpView */
/* @var $model common\models\PageItem */

$pageTitle = Yii::t('app', 'Update {modelClass}', [
        'modelClass' => Yii::t('app', 'Page Item'),
    ]) . ' ID: ' . $model->name;
$this->setBrowserTitle($pageTitle, 0);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Item Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="page-item-update">

    <h3 class="page-title">
        <?= Html::encode(Yii::t('app', 'Page Item Management')) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'pageTitle' => $pageTitle,
    ]) ?>

</div>
