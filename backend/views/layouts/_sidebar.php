<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/7/16 8:52 PM
 */
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <?php
        echo \enpii\enpiiCms\widgets\Menu::widget([
            'encodeLabels' => false,
            'submenuTemplate' => "\n<ul class=\"sub-menu\">\n{items}\n</ul>\n",
            'linkTemplate' => '<a href="{url}" class="{class}">{label}</a>',
            'labelTemplate' => '<span class="title">{label}</span>',
            'options' => [
                'id' => 'sidebar-primary-menu',
                'class' => 'page-sidebar-menu page-header-fixed side-primary-menu',
                'data-keep-expanded' => 'false',
                'data-auto-scroll' => 'true',
                'data-slide-speed' => 200,
            ],
            'itemOptions' => [
                'class' => 'nav-item',
            ],
            'items' => [
                [
                    'label' => '
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
                ',
                    'labelTemplate' => '{label}',
                    'options' => [
                        'class' => 'sidebar-toggler-wrapper hide',
                    ]
                ],

                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                [
                    'label' => '<i class="fa fa-tachometer"></i>
                    <span class="title">' . Yii::t(_NP_TEXT_CATE, 'Dashboard') . '</span>
                    <span class="arrow"></span>',
                    'url' => Yii::$app->urlManager->createUrl(''),
                    'labelTemplate' => '{label}',
                    'linkClass' => "nav-link nav-toggle",
                    'options' => [
                        'class' => "nav-item start",
                        'style' => 'margin-top: 42px;',
                    ],
                ],
                [
                    'label' => '<i class="icon-user"></i>
                    <span class="title">' . Yii::t(_NP_TEXT_CATE, 'User Management') . '</span>
                    <span class="arrow"></span>',
                    'url' => 'javascript:;',
                    'labelTemplate' => '{label}',
                    'linkClass' => "nav-link nav-toggle",
                    'options' => [
                        'class' => "nav-item start",
                    ],
                    'items' => [
                        [
                            'label' => '<i class="fa fa-list"></i> ' . Yii::t(_NP_TEXT_CATE, 'Standard User Listing'),
                            'url' => ['user/index']
                        ],
                        [
                            'label' => '<i class="fa fa-plus-square-o"></i> ' . Yii::t(_NP_TEXT_CATE, 'Create User'),
                            'url' => ['user/create']
                        ],
                    ]
                ],
                [
                    'label' => '<i class="icon-user"></i>
                    <span class="title">' . Yii::t(_NP_TEXT_CATE, 'Pin Management') . '</span>
                    <span class="arrow"></span>',
                    'url' => 'javascript:;',
                    'labelTemplate' => '{label}',
                    'linkClass' => "nav-link nav-toggle",
                    'options' => [
                        'class' => "nav-item start",
                    ],
                    'items' => [
                        [
                            'label' => '<i class="fa fa-list"></i> ' . Yii::t(_NP_TEXT_CATE, 'Coupon Pin Listing'),
                            'url' => ['coupon-pin/index']
                        ],
                        [
                            'label' => '<i class="fa fa-plus-square-o"></i> ' . Yii::t(_NP_TEXT_CATE, 'Create Coupon Pin'),
                            'url' => ['coupon-pin/create']
                        ],
                    ]
                ],

                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
            ],
        ]);
        ?>
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
