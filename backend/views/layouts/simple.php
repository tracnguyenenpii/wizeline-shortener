<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/16/16 5:07 PM
 */
/* @var $this \enpii\enpiiCms\libs\NpView */
/* @var $form \enpii\enpiiCms\libs\NpActiveForm */

use yii\helpers\Html;
use enpii\enpiiCms\libs\NpActiveForm;

\enpii\enpiiCms\webassets\fonts\assets\FontOpenSansAsset::register($this);
\enpii\enpiiCms\webassets\fonts\assets\FontAwesomeAsset::register($this);
\enpii\enpiiCms\webassets\plugins\bootstrapcustom\assets\BootstrapCustomAsset::register($this);
\enpii\enpiiCms\webassets\backend\style1\assets\EnpiiMainAsset::register($this);
\backend\assets\MainAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="<?= $this->getBodyClass() ?> relation">
<?php $this->beginBody() ?>

<?= \enpii\enpiiCms\widgets\BootstrapAlert::widget() ?>

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->

<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <?= $content ?>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

