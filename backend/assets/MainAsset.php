<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/13/16 3:15 PM
 */

namespace backend\assets;

use enpii\enpiiCms\libs\NpAssetBundle;

class MainAsset extends NpAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/main-custom.css',
    ];
    public $js = [
        'js/min/main-min.js',
        'js/min/main-custom-min.js',
    ];
}