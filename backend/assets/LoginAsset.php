<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/1/16 2:27 PM
 */

namespace backend\assets;

use enpii\enpiiCms\libs\NpAssetBundle;

class LoginAsset extends NpAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/login.css',
        'css/login-custom.css',
    ];
    public $js = [
        'js/min/login-min.js',
        'js/min/login-custom-min.js',
    ];
}