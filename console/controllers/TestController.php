<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/6/16 1:41 PM
 */

namespace console\controllers;


use common\libs\ClsControllerConsole;
use enpii\enpiiCms\models\User;

class TestController extends ClsControllerConsole
{
    public function actionIndex(){
        echo User::findByUsername('contact@enpii.com');
    }
}