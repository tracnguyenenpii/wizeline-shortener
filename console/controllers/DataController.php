<?php
/**
 * Created by PhpStorm.
 * Author: npbtrac@yahoo.com
 * Date time: 8/25/16 11:32 AM
 */

namespace console\controllers;


use common\libs\ClsControllerConsole;

class DataController extends ClsControllerConsole
{
    public $tableNameSite = 'site';
    public $tableNameUser = 'user';
    public $tableNameUserRole = 'user_role';
    public $tableNameUserProfile = 'user_profile';
    public $tableNameUserUserRoleRelation = 'user_user_role_relation';

    public function actionInitSeeding()
    {


    }

}

