<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coupon_pin`.
 */
class m161120_152453_create_shortener_item_table extends \enpii\enpiiCms\libs\NpMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%shortener_item}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(),
            'original_url' => $this->text(),
        ],$tableOptions);
        $this->addCreatedAt('shortener_item');
        $this->addUpdatedAt('shortener_item');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%shortener_item}}');
    }
}
